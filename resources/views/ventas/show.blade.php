@extends('layouts.appHome')
@section('titulo', 'Venta')
@section('contenido')
@include('ventas._menu')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">INFORMACIÓN DE LA VENTA</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-condensed">
      <tbody>
        <tr>
          <td width="200">
            <strong>No. ID:</strong>
          </td>
          <td>{{ $venta->id }}</td>
        </tr>
        <tr>
          <td>
            <strong>Nombre:</strong>
          </td>
          <td>{{ $venta->cliente->nombre }}</td>
        </tr>
        <tr>
          <td>
            <strong>Fecha Pago:</strong>
          </td>
          <td>{{ $venta->fechaPago }}</td>
        </tr>
        <tr>
          <td>
            <strong>CI ó NIT:</strong>
          </td>
          <td>{{ $venta->cliente->ci }}</td>
        </tr>
        <tr>
          <td>
            <strong>Venta registrada:</strong>
          </td>
          <td>{{ $venta->created_at }}</td>
        </tr>
        <tr>
          <td colspan="2">
            <table class="table table-striped table-hover table-bordered">
              <thead>
                <tr>
                  <th width="100">#</th>
                  <th>Producto</th>
                  <th with="100" align="center">Precio</th>
                </tr>
              </thead>
              <tbody>
                @foreach($atenciones as $atencion)
                <tr>
                  <td>
                    {{ $atencion->id }}
                  </td>
                  <td>
                    {{ $atencion->servicio->nombreServicio }}
                  </td>
                  <td with="100" align="right">
                    {{ $atencion->servicio->precio }}
                  </td>
                </tr>
                @endforeach
                @foreach($consultas as $consulta)
                <tr>
                  <td>
                    {{ $consulta->id }}
                  </td>
                  <td>
                    Consulta
                  </td>
                  <td with="100" align="right">
                    50
                  </td>
                </tr>
                @endforeach
                @foreach($internaciones as $internacion)
                <tr>
                  <td>
                    {{ $internacion->id }}
                  </td>
                  <td>
                    Internacion: {{ $internacion->motivo }}
                  </td>
                  <td with="100" align="right">
                    100
                  </td>
                </tr>
                @endforeach
                @foreach($vacunas as $vacuna)
                <tr>
                  <td>
                    {{ $vacuna->id }}
                  </td>
                  <td>
                    {{ $vacuna->nombre }}
                  </td>
                  <td with="100" align="right">
                    10
                  </td>
                </tr>
                @endforeach
                @foreach($productos as $producto)
                <tr>
                  <td>{{ $producto->id }}</td>
                  <td>{{ $producto->nombre }}</td>
                  <td with="100" align="right">{{ $producto->precio }}</td>
                </tr>
                @endforeach
                <tr>
                  <td>
                    <h4>Total</h4>
                  </td>
                  <td></td>
                  <td with="100" align="right"><h4>{{ $venta->total }}</h4></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
</blockquote>
@endsection
