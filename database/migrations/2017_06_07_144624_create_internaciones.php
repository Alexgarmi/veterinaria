<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInternaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('motivo');
            $table->date('fechaIngreso')->nullable();
            $table->date('fechaSalida')->nullable();
            $table->boolean('pendiente')->default(0);
            $table->integer('id_animal')->unsigned();
            $table->integer('id_user')->unsigned();

            $table->timestamps();

            $table->foreign('id_animal')
                ->references('id')
                ->on('animales');
                
            $table->foreign('id_user')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internaciones');
    }
}
