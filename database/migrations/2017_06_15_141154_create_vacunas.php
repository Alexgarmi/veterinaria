<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacunas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacunas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->date('fechaAplicacion');
            $table->boolean('aplicado')->default(0);
            $table->boolean('pendiente')->default(0);
            $table->integer('id_animal')->unsigned();
            $table->integer('id_user')->unsigned();

            $table->timestamps();

            $table->foreign('id_animal')
                ->references('id')
                ->on('animales');
                
            $table->foreign('id_user')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
