@extends('layouts.appHome')
@section('titulo', 'Internacion')
@section('contenido')
@include('internaciones._menuEditar')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DE LA INTERNACIÓN</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $internacion->id }}</td>
        </tr>
        <tr>
          <td>Motivo:</td>
          <td>{{ $internacion->motivo }}</td>
        </tr>
        <tr>
          <td>Fecha de Ingreso:</td>
          <td>{{ $internacion->fechaIngreso }}</td>
        </tr>
        <tr>
          <td>Fecha de Salida:</td>
          <td>{{ $internacion->fechaSalida }}</td>
        </tr>
        <tr>
          <td>Animal:</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$internacion->animal->id]) }}">
              {{ $internacion->animal->id }}: {{ $internacion->animal->nombre }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $internacion->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>

    @endsection
