  <div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">CREAR/EDITAR VACUNA</h3>
  </div>
  <div class="panel-body">
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Ingresar Datos de la Vacuna - No. {{ $vacuna->id }}:</div>
        <div class="panel-body">
          @if( $vacuna-> exists )
            <form action="{{ route('update_vacuna_path',['vacuna' => $vacuna->id]) }}" class="form-horizontal" method="POST" role="form">
            {{ method_field('PUT') }}
          @else
            <form action="{{ route('store_vacunas_path') }}" class="form-horizontal" method="POST" role="form">
          @endif
            {{ csrf_field() }}
              <fieldset>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="nombre">Nombre</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="nombre" name="nombre" placeholder="Motivo de la vacuna" type="text" value="{{ $vacuna->nombre or old('nombre') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="fechaAplicacion">Aplicacion</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="fechaAplicacion" name="fechaAplicacion" placeholder="AAAA-MM-DD" type="date" value="{{ $vacuna->fechaAplicacion or old('fechaAplicacion') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="aplicado">Aplicado</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                      </div>
                      <select class="form-control" id="aplicado" name="aplicado"/>
                        <option value="0" {{!$vacuna->aplicado or old('aplicado')? 'selected': ''}} >No</option>
                        <option value="1" {{$vacuna->aplicado or old('aplicado')? 'selected': ''}} >Si</option>
                      <select>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="id_animal">Nombre de la Mascota </label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </div>
                      @if($vacuna->exists)
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $vacuna->animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $vacuna->animal->id or old('id_animal') }}"/>
                      @else
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $animal->id or old('id_animal') }}"/>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    @if($vacuna->exists)
                      <a href="{{ route('vacuna_path',['vacuna' =>$vacuna->id]) }}" class="btn btn-default">Cancelar</a>
                    @else
                      <a href="{{ route('animal_path',['animal' =>$animal->id]) }}" class="btn btn-default">Cancelar</a>
                    @endif
                    <button class="btn btn-primary" type="submit">Guardar Vacuna</button>
                  </div>
                </div>
              </fieldset>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>
</div>