@extends('layouts.appHome')
@section('titulo', 'Registrar Atención')
@section('contenido')
@include('atenciones._menu')
@include('layouts._messages')
@include('layouts._errors')

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title"  align="center">INGRESAR NUEVA ATENCIÓN</h3>
  </div>
  <div class="panel-body">

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Datos de la Atención:</div>
        <div class="panel-body">
          <form class="form-horizontal" action="{{ route('store_atenciones_path') }}" method="POST" role="form">
            {{ csrf_field() }}
            <fieldset>
              <div class="form-group ">
                <label class="control-label col-lg-2" for="id_animal">Mascota</label>
                <div class="col-lg-10">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </div>
                    <select class="select form-control" id="id_animal" name="id_animal">
                      @foreach($animales as $animal)
                      <option value="{{ $animal->id }}">
                        {{ $animal->nombre }}
                        @endforeach
                      </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group ">
                <label class="control-label col-lg-2" for="id_servicio">Servicio</label>
                <div class="col-lg-10">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-bath" aria-hidden="true"></i>
                    </div>
                    <select class="select form-control" id="id_servicio" name="id_servicio">
                      @foreach($servicios as $servicio)
                      <option value="{{ $servicio->id }}">
                        {{ $servicio->nombreServicio }}
                      </option>
                      @endforeach
                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-10 col-lg-offset-2">
                <a href="{{ route('atenciones_path')}}" class="btn btn-default">Cancelar</a>
                <button type="submit" class="btn btn-primary">Registrar Atencion</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>

  </div>
</div>



@endsection
