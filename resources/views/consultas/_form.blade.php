<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">CREAR NUEVA CONSULTA</h3>
  </div>
  <div class="panel-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @include('layouts._errors')
          <div class="panel panel-default">
            <div class="panel-body">
              @if( $consulta->exists )
                <form action="{{ route('update_consulta_path',['consulta' =>$consulta->id]) }}" class="form-horizontal" method="POST" role="form">
                {{ method_field('PUT') }}
              @else
                <form action="{{ route('store_consultas_path') }}" class="form-horizontal" method="POST" role="form">
              @endif
              {{ csrf_field() }}
                  <fieldset>

                  <ul class="list-group">
                    <li class="list-group-item">
                      <i class="fa fa-user-md" aria-hidden="true"></i> <strong>Médico: </strong>
                      {{ Auth::user()->name }}
                      <input type="hidden" name="id_user" value="{{ Auth::user()->id }}"/>
                    </li>

                    <div class="form-group ">
                      <label class="control-label col-lg-2" for="id_animal">Nombre de la Mascota </label>
                      <div class="col-lg-10">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i aria-hidden="true" class="fa fa-user aria-hidden="true""></i>
                          </div>
                          @if($consulta->exists)
                            <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $consulta->animal->nombre}}"/>
                            <input type="hidden" name="id_animal" value="{{ $consulta->animal->id or old('id_animal') }}"/>
                          @else
                            <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $animal->nombre}}"/>
                            <input type="hidden" name="id_animal" value="{{ $animal->id or old('id_animal') }}"/>
                          @endif
                        </div>
                      </div>
                    </div>
                   
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="diagnostico">Diagnóstico:</label>
                      <div class="col-lg-10">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          </div>
                          <textarea class="form-control" rows="3" id="diagnostico" name="diagnostico">
                          </textarea>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="receta">Receta:</label>
                      <div class="col-lg-10">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          </div>
                          <textarea class="form-control" rows="3" id="receta" name="receta"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-lg-2 control-label" for="observaciones">Observaciones:</label>
                      <div class="col-lg-10">
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          </div>
                          <textarea class="form-control" rows="3" id="observaciones" name="observaciones"></textarea>
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                        @if($consulta->exists)
                          <a href="{{ route('consulta_path',['consulta' =>$consulta->id]) }}" class="btn btn-default">Cancelar</a>
                        @else
                          <a href="{{ route('animal_path',['animal' =>$animal->id]) }}" class="btn btn-default">Cancelar</a>
                        @endif
                        <button class="btn btn-primary" type="submit">Guardar Consulta</button>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
