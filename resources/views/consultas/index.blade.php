@extends('layouts.appHome')
@section('titulo', 'Consultas')
@section('contenido')
@include('consultas._menu')

<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LISTADO DE CONSULTAS</h3>
  </div>
  <div class="panel-body table-responsive">
  @include('layouts._messages')
  @include('layouts._errors')
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No. ID</th>
          <th>Paciente</th>
          <th>Fecha / Hora</th>
          <th>Médico</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($consultas as $consulta)
        <tr>
          <td>{{ $consulta->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$consulta->animal->id]) }}">  
              {{ $consulta->animal->nombre }}
            </a>
          </td>
          <td>{{ $consulta->created_at }}</td>
          <td>{{ $consulta->user->name }}</td>
          <td><a href="{{ route('consulta_path',['consulta' =>$consulta->id]) }}" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Ver</a></td>
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $consultas->render() }}
  </div>
</div>
@endsection

