@extends('layouts.appHome')
@section('titulo', 'Crear Consulta')
@section('contenido')
@include('consultas._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('consultas._form', ['consulta' => $consulta,'animal' => $animal])
@endsection
