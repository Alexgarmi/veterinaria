<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">CREAR NUEVO CLIENTE</h3>
  </div>
  <div class="panel-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @include('layouts._errors')
          <div class="panel panel-default">
            <div class="panel-heading">Ingresar Datos del Cliente - No. {{ $cliente->id }}:</div>
            <div class="panel-body">
              @if( $cliente->exists )
                <form action="{{ route('update_cliente_path',['cliente' =>$cliente->id]) }}" class="form-horizontal" method="POST" role="form">
                {{ method_field('PUT') }}
              @else
                <form action="{{ route('store_clientes_path') }}" class="form-horizontal" method="POST" role="form">
              @endif
              {{ csrf_field() }}
              <fieldset>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="nombre">Nombre *</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-user" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" name="nombre" placeholder="Nombre del cliente" type="text" value="{{ $cliente->nombre or old('nombre') }}">
                      </input>
                    </div>
                  </div>
                </div>
              <div class="form-group">
                <label class="col-lg-2 control-label" for="ci">CI ó NIT *</label>
                <div class="col-lg-10">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-id-card-o" aria-hidden="true"></i>
                    </div>
                    <input class="form-control" name="ci" placeholder="CI ó NIT del cliente" type="text" value="{{ $cliente->ci or old('ci') }}">
                  </input>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label" for="email">Correo electrónico *</label>
              <div class="col-lg-10">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                  <input class="form-control" name="email" placeholder="Correo electrónico del cliente" type="text" value="{{ $cliente->email or old('email') }}">
                </input>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label" for="telefono">Teléfono</label>
            <div class="col-lg-10">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <input class="form-control" name="telefono" placeholder="Teléfono del cliente" type="telefono" value="{{ $cliente->telefono or old('telefono') }}">
              </input>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-2 control-label" for="direccion">Dirección</label>
          <div class="col-lg-10">
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
              </div>
              <input class="form-control" name="direccion" placeholder="Dirección" type="telefono" value="{{ $cliente->direccion or old('direccion') }}">
            </input>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
        @if($cliente->id == null)        
          <a href="{{ route('clientes_path') }}" class="btn btn-default">Cancelar</a>
        @else
          <a href="{{ route('cliente_path',['cliente' =>$cliente->id]) }}" class="btn btn-default">Cancelar</a>
        @endif
          <button class="btn btn-primary" type="submit">Guardar Cliente</button>
        </div>
      </div>
    </fieldset>
  </form>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

