@extends('layouts.appHome')
@section('titulo', 'Productos')
@include('ventas._menuListaProductos')
@section('contenido')
@include('layouts._messages')
@include('layouts._errors')

<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">LISTADO DE PRODUCTOS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-hover table-striped ">
      <thead>
        <tr>
          <th>#</th>
          <th>Producto</th>
          <th>Precio</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($productos as $producto)
        <tr>
          <td>{{ $producto->id }}</td>
          <td>
            <h5>{{ $producto->nombre }}</h5>
          </a>
        </td>
        <td>{{ $producto->precio }}</td>
        <td>
          <span>
            <form action="{{ route('store_producto_path',['venta'=>$venta->id, 'producto'=>$producto->id]) }}" method="POST">
              {{ csrf_field() }}
              <button type="submit" class="btn btn-warning btn-xs">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                Agregar
              </button>
            </form>
          </span>
        </td>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</div>
@endsection
