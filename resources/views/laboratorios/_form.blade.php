<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">CREAR/EDITAR LABORATORIO</h3>
  </div>
  <div class="panel-body">
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Ingresar Datos de la Laboratorio - No. {{ $laboratorio->id }}:</div>
        <div class="panel-body">
          @if( $laboratorio-> exists )
            <form action="{{ route('update_laboratorio_path',['laboratorio' => $laboratorio->id]) }}" class="form-horizontal" method="POST" role="form">
            {{ method_field('PUT') }}
          @else
            <form action="{{ route('store_laboratorios_path') }}" class="form-horizontal" method="POST" role="form">
          @endif
            {{ csrf_field() }}
              <fieldset>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="tipoExamen">Tipo de Examen</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-heartbeat" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="tipoExamen" name="tipoExamen" placeholder="Tipo de examen" type="text" value="{{ $laboratorio->tipoExamen or old('tipoExamen') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="muestra">Muestra</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-thermometer-full" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="muestra" name="muestra" placeholder="Muestra" type="text" value="{{ $laboratorio->muestra or old('muestra') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="resultado">Resultado</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="resultado" name="resultado" placeholder="Resultado" type="text" value="{{ $laboratorio->resultado or old('resultado') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="documentoDigital">Documento</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-image-o" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="documentoDigital" name="documentoDigital" placeholder="Resultado" type="file" value="{{ $laboratorio->documentoDigital or old('documentoDigital') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="id_animal">Nombre de la Mascota </label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                       <i class="fa fa-twitter" aria-hidden="true"></i>
                      </div>
                      @if($laboratorio->exists)
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $laboratorio->animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $laboratorio->animal->id or old('id_animal') }}"/>
                      @else
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $animal->id or old('id_animal') }}"/>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    @if($laboratorio->exists)
                      <a href="{{ route('laboratorio_path',['laboratorio' =>$laboratorio->id]) }}" class="btn btn-default">Cancelar</a>
                    @else
                      <a href="{{ route('animal_path',['animal' =>$animal->id]) }}" class="btn btn-default">Cancelar</a>
                    @endif
                    <button class="btn btn-primary" type="submit">Guardar Laboratorio</button>
                  </div>
                </div>
              </fieldset>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>
</div>