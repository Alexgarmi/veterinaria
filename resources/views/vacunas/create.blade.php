@extends('layouts.appHome')
@section('titulo', 'Registrar Internacion')
@section('contenido')
@include('vacunas._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('vacunas._form',['vacuna' => $vacuna, 'animal' => $animal])
@endsection