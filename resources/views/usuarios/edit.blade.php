@extends('layouts.appHome')
@section('titulo', 'Editar Usuario')
@section('contenido')
@include('usuarios._menuUsuario')
@include('layouts._messages')
@include('layouts._errors')
@include('usuarios._form',['usuario' => $usuario])
@endsection
