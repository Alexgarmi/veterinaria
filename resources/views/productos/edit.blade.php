@extends('layouts.appHome')

@section('titulo', 'Editar Producto')
@section('contenido')
@section('tituloCentral', 'EDITAR DATOS DEL PRODUCTO')
@include('productos._menu')
@include('layouts._messages')
@include('layouts._errors')
	
@include('productos._form',['producto' => $producto])


@endsection