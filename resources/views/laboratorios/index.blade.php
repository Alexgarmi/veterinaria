@extends('layouts.appHome')
@section('titulo', 'Laboratorios')
@section('contenido')
@include('laboratorios._menu')

<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LISTADO DE LABORATORIOS</h3>
  </div>
  <div class="panel-body table-responsive">
  @include('layouts._messages')
  @include('layouts._errors')
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Paciente</th>
          <th>Tipo Examen</th>
          <th>Muestra</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($laboratorios as $laboratorio)
        <tr>
          <td>{{ $laboratorio->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$laboratorio->animal->id]) }}">  
              {{ $laboratorio->animal->nombre }}
            </a>
          </td>
          <td>{{ $laboratorio->tipoExamen }}</td>
          <td>{{ $laboratorio->muestra }}</td>
          <td>
            <a href="{{ route('laboratorio_path',['laboratorio' => $laboratorio->id]) }}" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Ver</a>
          </td>
          <td>
          </td>
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $laboratorios->render() }}
  </div>
</div>
@endsection
<br>
<br>

