<?php

namespace Veterinaria;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';

	protected $fillable = ['nombre', 'precio', 'stock'];
}
