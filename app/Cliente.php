<?php

namespace Veterinaria;

use Veterinaria\Animal;
use Veterinaria\Venta;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model{

	use DatesTranslator;

	protected $table = 'clientes';

	protected $fillable = ['nombre', 'ci', 'email', 'telefono', 'direccion'];
	//Veterinaria\Cliente::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'])

	// para identificar las columnas que sea necesario llenarlas
	//protected $guarded = [];

	public function animales()
	{
		return $this->hasMany('Veterinaria\Animal', 'id_cliente');
	}

	public function ventas()
	{
    	return $this->hasMany('Veterinaria\Venta', 'id_venta');
	}

	public function proximasVacunas() {
		$vacunas = $this->animales[1]->vacunas->where('aplicado',true);
		return $vacunas;
	}

	public function proximasInternaciones() {
		$vacunas = $this->animales[1]->vacunas->where('aplicado',true);
		return $vacunas;
	}
}



?>