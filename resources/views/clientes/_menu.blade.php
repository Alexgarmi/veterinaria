<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    <a href="{{ route('create_cliente_path') }}">
      <span class="text-primary"><i class="fa fa-user-plus" aria-hidden="true"></i>
      Nuevo Cliente</span>
    </a>
  </li>
  <li class="{{ Request::path() == 'clientes' ? 'active' : '' }}">
    <a href="{{ route('clientes_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Clientes</span>
    </a>
  </li>
<!--  
  <li class="disabled text-info">
    <a href="">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Cliente</span>
    </a>
  </li>
-->  
</ul>

 