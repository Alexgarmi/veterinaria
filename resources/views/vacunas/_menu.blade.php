<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'vacunas' ? 'active' : '' }}">
    <a href="{{ route('vacunas_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Vacunas</span>
    </a>
  </li>
<!--
  @ if(Auth::user()->hasAnyRole(['administrador']))
  <li class="{ { Request::path() == 'vacunas' ? 'disabled text-info' : '' }}">
    <a href="#">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Vacuna</span>
    </a>
  </li>
  @ endif
-->  
</ul>
