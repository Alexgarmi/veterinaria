@extends('layouts.appHome')
@section('titulo', 'Productos')
@section('contenido')
@include('productos._menu')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">LISTADO DE PRODUCTOS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-hover ">
      <thead>
        <tr>
          <th>#</th>
          <th>Producto</th>
          <th>Stock</th>
          <th>Precio</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($productos as $producto)
        <tr>
          <td>{{ $producto->id }}</td>
          <td>
            <a href="{{ route('producto_path',['producto' =>$producto->id]) }}">
              {{ $producto->nombre }}
            </a>
          </td>
          <td>{{ $producto->stock }}</td>
          <td>{{ $producto->precio }}</td>
          <td>
            <span>
              @if(Auth::user()->
              hasAnyRole(['administrador']))
              <a href="{{ route('edit_producto_path', ['producto' =>$producto->id]) }}" class="btn btn-info btn-link btn-xs">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                Editar
              </a>
              @endif
            </span>
          </td>
<!--        
          <td>
            <span>
              @ (Auth::user()->hasAnyRole(['administrador']))
              <form action="{ { route('delete_producto_path',['producto'=>$producto->id]) }}" method="POST">
                { { csrf_field() }}
                { { method_field('DELETE') }}
                <button type="submit"  class="btn btn-link btn-xs">
                  <spam class="text-danger">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                    Eliminar
                  </spam>
                </button>
              </form>
              @ endif
            </span>
          </td>
-->          
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{ $productos->render() }}
</div>
</div>
@endsection
