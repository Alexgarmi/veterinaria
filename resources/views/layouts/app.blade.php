<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
  </head>
  <body background="{{ asset('img/fondo-veterinaria.png') }}" class="img-responsive">
    <div class="container theme-showcase" role="main">
      <div id="app">
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <!-- Collapsed Hamburger -->
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- Branding Image -->
              @if(Auth::user())

                <a class="navbar-brand" href="{{ url('/home') }}"><i class="fa fa-ambulance" aria-hidden="true"></i> {{ config('app.name', 'Veterinaria') }} 

              @else              
              <a class="navbar-brand" href=" {{ url('/') }}"><i class="fa fa-ambulance" aria-hidden="true"></i> {{ config('app.name', 'Veterinaria') }} </a>
              @endif
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
              <!-- Left Side Of Navbar -->
              <ul class="nav navbar-nav">&nbsp;</ul>
              <!-- Right Side Of Navbar -->
              <ul class="nav navbar-nav navbar-right">
                <li>
                  <a href="{{ route('webCliente_path') }}"><strong><i class="fa fa-key" aria-hidden="true"></i> Acceso para Clientes</a></strong>
                </li>
                <!-- Authentication Links -->
                @if (Auth::guest())
                <li>
                  <a href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i> Iniciar sesión</a>
                </li>
<!--
                <li>
                  <a href="{ { route('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Registrarse</a>
                </li>
-->
                @else
                @include('layouts._usuario')
                @endif
              </ul>
            </div>
          </div>
        </nav>
        @yield('contenido')
      </div>
    </div>
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  </body>
</html>
<br/>
<div class="navbar navbar-inverse navbar-fixed-bottom">
  <footer>
    <h5 align="center">
      <em>
        Copyrigth FCyT - Lic. en Ingenieria Informática -
        <strong>Rene Copaga, Alex Garcia</strong>
        - Aplicaciones Web Avanzada Semestre 2017-1
      </em>
    </h5>
  </footer>
</div>
</body>
</html>
