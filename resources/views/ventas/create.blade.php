@extends('layouts.appHome')

@section('titulo', 'Crear Venta')

@section('contenido')
@include('ventas._menu')

@include('layouts._messages')
@include('layouts._errors')
@include('ventas._formCliente',['clientes' => $clientes])

@endsection