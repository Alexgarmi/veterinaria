<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}"/>
  <title>{{ config('app.name', 'Laravel') }} - @yield('titulo')</title>
  <!-- Styles -->
  <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('css/style.css') }}" rel="stylesheet"/>
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
</head>

<body background="{{ asset('img/fondo-veterinaria.png') }} ">
  <div class="container theme-showcase" role="main">
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Branding Image -->
          <a class="navbar-brand" href="{{ url('/home') }}">
          <span class="text"><i class="fa fa-ambulance" aria-hidden="true"></i>
           {{ config('app.name', 'Veterinaria') }}</span></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            @if(Auth::user()->hasAnyRole(['administrador','veterinario','recepcion','caja']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Clientes
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('clientes_path') }}">Listar Clientes</a>
                </li>
                <li>
                  <a href="{{ route('create_cliente_path') }}">Nuevo Cliente</a>
                </li>
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','veterinario','empleado','recepcion']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Animales
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('animales_path') }}">Listar Animales</a>
                </li>
 <!-- La opción de añadir nuevo nimal solo dentro del cliente
                <li>
                  <a href="{ { route('create_animal_path') }}">Nuevo Animal</a>
                </li>
-->                
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','veterinario','recepcion']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Clínica
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('consultas_path') }}">Consultas</a>
                </li>
                <li>
                  <a href="{{ route('internaciones_path') }}">Internaciones</a>
                </li>
                <li>
                  <a href="{{ route('laboratorios_path') }}">Laboratorios</a>
                </li>
                <li>
                  <a href="{{ route('vacunas_path') }}">Vacunas</a>
                </li>
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','veterinario','empleado','recepcion']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Atenciones
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('atenciones_path') }}">Historial de Atenciones</a>
                </li>
                @if(Auth::user()->hasAnyRole(['administrador']))
                <li>
                  <a href="{{ route('create_atencion_path') }}">Nueva Atención</a>
                </li>
                @endif
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','almacen','recepcion']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Productos
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('productos_path') }}">Listar Productos</a>
                </li>
              @if(Auth::user()->hasAnyRole(['administrador']))
                <li>
                  <a href="{{ route('create_producto_path') }}">Nuevo Producto</a>
                </li>
              @endif
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','empleado','recepcion', 'caja']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Ventas
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('ventas_path') }}">Listar Ventas</a>
                </li>
                <li>
                @if(Auth::user()->hasAnyRole(['administrador', 'caja']))
                  <a href="{{ route('create_venta_path') }}">Nueva Venta</a>
                @endif
                </li>
              </ul>
            </li>
            @endif
            @if(Auth::user()->hasAnyRole(['administrador','almacen','caja']))
            <li>
              <a href="{{ route('reporte_dias_path') }}">Reportes</a>
            </li>
            @endif
<!--              
            @ if(Auth::user()->hasAnyRole(['administrador']))
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Usuarios
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{ { route('usuarios_path') }}">Listar Usuarios</a>
                </li>
                <li>
                  <a href="{ { route('create_usuario_path') }}">Nuevo Usuario</a>
                </li>
              </ul>
            </li>
            @ endif
-->              
          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="nav navbar-nav navbar-right">
            <!-- Authentication Links -->
            @if (Auth::guest())
            <li>
              <a href="{{ route('login') }}">Ingresar</a>
            </li>
            <li>
              <a href="{{ route('register') }}">Registrarse</a>
            </li>
            @else
            @include('layouts._usuario')
            @endif
          </ul>
        </div>
        <!-- /.nav-collapse  -->
      </div>
    </nav>
    <br/>
    @yield('contenido')
  </div>
  <br/>
  <br/>
  <br/>
  <div class="navbar navbar-inverse navbar-fixed-bottom">
    <h5 align="center">
      <em>
        Copyrigth FCyT - Lic. en Ingenieria Informática -
        <strong>Rene Copaga, Alex Garcia</strong>
        - Aplicaciones Web Avanzada Semestre 2017-1
      </em>
    </h5>
  </div>
  <!-- Scripts -->
  <script src=" {{ asset('js/jquery.min.js') }}"></script>
  <script src=" {{ asset('js/bootstrap.min.js') }}"></script>
  <script src=" {{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src=" {{ asset('js/bootbox.min.js') }}"></script>
</body>
</html>


