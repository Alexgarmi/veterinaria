<ul class="nav nav-tabs">

  <li class="{{ Request::path() == 'ventas' ? 'active' : '' }}">
  	<a href="{{ route('ventas_path')}}">
  	<span class="text-primary"> <i class="fa fa-th-list" aria-hidden="true"></i> Listar Ventas</a>
  </li>

@if(Auth::user()->hasAnyRole(['administrador', 'caja']))
  <li class="{{ Request::path() == 'ventas/create' ? 'active' : '' }}">
  	<a href="{{ route('create_venta_path')}}">
  	<span class="text-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nueva Venta</span>
  	</a>
  </li>
@endif

</ul>
