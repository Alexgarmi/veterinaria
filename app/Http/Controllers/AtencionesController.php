<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Servicio;
use Veterinaria\Atencion;
use Illuminate\Http\Request;

class AtencionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $atenciones = Atencion::orderBy('id', 'desc')->paginate(4);

        return view('atenciones.index')->with(['atenciones' => $atenciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animales = Animal::all();
        $servicios = Servicio::all();
        return view('atenciones.create')->with(['animales' => $animales, 'servicios' => $servicios]);
    }

    public function store(Request $request)
    {
        $atencion = new Atencion;
        $atencion->id_animal = $request->get('id_animal');
        $atencion->id_servicio = $request->get('id_servicio');
        $atencion->id_user = $request->user()->id;
        $atencion->pendiente = true;
        $atencion->save();

        session()->flash('message', 'La atención fue agregada a la Base de Datos!');

        return redirect()->route('atenciones_path'); 
    }
}