<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    <a href="{{ route('create_cliente_path') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo Cliente</a>
  </li>
  <li class="{{ Request::path() == 'clientes' ? 'active' : '' }}">
    <a href="{{ route('clientes_path') }}"><i class="fa fa-list-ul" aria-hidden="true"></i> Listar Clientes</a>
  </li>
  <li class="active">
    <a href=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Cliente</a>
  </li>
</ul>
