<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'consultas' ? 'active' : '' }}">
    <a href="{{ route('consultas_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Consultas</span>
    </a>
  </li>
<!--
  @ if(Auth::user()->hasAnyRole(['administrador']))
  <li class="text-info">
    <a href="">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Consulta</span>
    </a>
  </li>
  @ endif
-->  
</ul>

 