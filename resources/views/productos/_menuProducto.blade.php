<ul class="nav nav-tabs">

@if(Auth::user()->hasAnyRole(['administrador','almacen']))
  <li class="{{ Request::path() == 'productos/create' ? 'active' : '' }}">
  	<a href="{{ route('create_producto_path')}}">
  	<span class="text-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Producto</span>
  	</a>
  </li>
@endif

  <li class="{{ Request::path() == 'productos' ? 'active' : '' }}">
  	<a href="{{ route('productos_path')}}">
  	<span class="text-primary"><i class="fa fa-th-list" aria-hidden="true"></i> Listar Productos</span>
  	</a>
  </li>
@if(Auth::user()->hasAnyRole(['administrador','almacen']))
  <li class="{{ Request::path() == 'productos' ? 'disabled text-info' : '' }}">
    <a href="{{ route('edit_producto_path', ['producto' =>$producto->id]) }}">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Producto</span>
    </a>
  </li>
@endif
</ul>
