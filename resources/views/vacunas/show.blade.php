@extends('layouts.appHome')
@section('titulo', 'Vacuna')
@section('contenido')
@include('vacunas._menuEditar')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DE LA VACUNA</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $vacuna->id }}</td>
        </tr>
        <tr>
          <td>Nombre:</td>
          <td>{{ $vacuna->nombre }}</td>
        </tr>
        <tr>
          <td>Fecha de Aplicacion:</td>
          <td>{{ $vacuna->fechaAplicacion }}</td>
        </tr>
        <tr>
          <td>Aplicado</td>
          <td>{{ $vacuna->aplicado ? 'Si' : 'No' }}</td>
        </tr>
        <tr>
          <td>Animal:</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$vacuna->animal->id]) }}">
              {{ $vacuna->animal->id }}: {{ $vacuna->animal->nombre }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $vacuna->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>

    @endsection
