<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Veterinaria\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Veterinaria\Cliente::class, function (Faker\Generator $faker) {
    $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
    return [
        'nombre' => $faker->name,
        'ci' => $faker->numberBetween($min = 100000, $max = 9999999),
        'email' => $faker->unique()->freeEmail,
        'telefono' => $faker->phoneNumber,
        'direccion' => $faker->streetAddress,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Veterinaria\Animal::class, function (Faker\Generator $faker) {
    $faker->addProvider(new Faker\Provider\es_ES\Person($faker));
    return [
        'nombre' => $faker->randomElement($array = array ('Lulu','Lucas','Muñeca','Cesar','Lupe','Patan','Axel','Arry','Anita','Aly','Amor','Ari','Also','Alen','Atril','Asterix','Ander','Afer','Anúbi','Ambar','Andy','Aroa','Angie','Atila','Asli','Atenea','Aga','Alba','Arshen','Arare','Atos','Arcos','Argón','Alan','Angus','Alaska','Anuka','Asía','Afrika','Alana','Ada','Abril','Argi','Alvin','Azkar','Achi','Artza','Aike','Anelka','Argos','Akane','Bibi','Bily','Buba','Buda','Bizkor','Barbie','Bures','Baba','Baby','Baco','Bach','Bali','Balin','Baron','Boy','Bart','Bella','Berta','Barney','Bono','Bruna','Baby','Blanco','Big','Biggie','Bella','Blondi','Bebe','Bana','Buki','Bruce','Bob','Bicho','Brisa','Bella','Bob','Burn','Ben','Black','Boris','Blacky','Beto','Bongo','Bono','Berni','Bosqui','Blacki','Beltza','Beltzi','Blanqui','Blue','Bella','Botitas','Bombon','Bequer','Blacky','Bolo','Braulio','Chocolate','Charlie','Cejas','Canela','Camila','Cora','Charlie','Caty','Clara','Cindy','Cookie','Candy','Camila','Claudio','Chacho','Currito','Chispita','Churri','Che','Cie','Cuki','Chiqui','Chispa','Cicuta','Cooper','Cacho','Cosa','Cara','Cati','Chata','Copito','Colorin','Canelin','Calcetines','Celia','Dara','Duquesa','Deisy','Danger','Dan','Dixie','Dama','Diana','Dolça','Duna','Dalai','Dani','Dirk','Dumas','Dominic','Dino','Darko','Dulcinea','Denébola','Dama','Dardo','Deus','Dino','Diana','Moli','Mimi','Miki','Mini','Max','Misi','Mister','Mafalda','Manchas','Mango','Mili','Milú','Maestro','Magnolia','Mamba','Mambo','Moreno','Moose','Morris','Mauricio','Mad','Max','Mafi','Magy','Maiky','Majo','Maki','Mickey','Miniee','Manchita','Malaka','Mami','Marilyn','Marlon','Marx','Paris','Polo','Petunia','Platon','Pachi','Percy','Picasso','Pooh','Pinky','Piwi','Pinocchio','Pizza','Pluto','Platón','Poppye','Pretty','Pop','Pirata','Poli','Pate','Panda','Paca','Pachi','Pam','Palo','Ross','Randy','Ron','Riky','Rufo','Romeo','Rambo','Ramses','Remy','Ricky','Robin','Rubi','Rocky','Rino','Rio','Romeo','Rex','Roma','Rajah','Rabitt','Raby','Ralf','Rando','Ravel','Rao','Reina','Remo','Renoir','Robby','Rocco','Rotti','Rumba','Raquel','Reiki','Rekke','Rem','Roco','Rocio','Racu','Roky','Riki','Roxy','Roly','Rubiales','Rubio','Ruter','Ralir','Rolirtan','Rilk','Rapshy','Rosi','Remo','Romina','Raissa','Ruperto','Rufi','Rafa','Rusty','Tor','Tadeo','Tammy','Tamu','Tara','Tati','Tato','Teddy','Teki','Terca','Terry','Tiara','Tibet','Tibo','Tic','Tika','Tinta','Titan','Tio','Tigre','Tom','Truco','Trulli','Tarzan','Tabu')
            ),
        'especie' => $faker->randomElement($array = array ('Mamíferos','Aves','Peces','Reptiles','Anfibios')
            ),
        'raza' => $faker->randomElement($array = array ('Perro','Gato','Conejo','Hámster','Jerbo','Chinchilla','Ardilla','Ratón','Rata','Uron','Erizo','Caballo','Cerdo','Canario','Pariquito','Cotorra','Cacatua','Loro','Guacamayo','Gallina','Pato','Pavo real','Carpa','Guppy','Pez Arquero','Pez rojo','Pez payaso','Piraña','Platy','Tortuga de agua','Tortuga de tierra','Serpiente','Iguana','Geko','Cocodrilo','Camáleon','Rana','Sapo','Sapo gigante')
            ),
        'genero' => $faker->randomElement($array = array ('Hembra','Macho')
            ),
        'fechaNacimiento' => $faker->dateTimeThisDecade($max = 'now', $timezone = date_default_timezone_get()
            ),
        'id_cliente' => $faker->numberBetween($min = 5, $max = 30
            ),
        
        //Para crear clientes con animales
        //   function(){ return factory(Veterinaria\Cliente::class)->create()->id; }
        
        //Ejecutar en tinker: 
        // factory(Veterinaria\Cliente::class, 30)->create();
        // factory(Veterinaria\Animal::class, 40)->create();
        // factory(Veterinaria\Producto::class, 20)->create();

    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Veterinaria\Producto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement($array = array ('Vacuna','AlimentoDog','AlimentoCat','AlimentoAve','Plato','Correa','Medicamento 1','Medicamento 2')),
        'precio' => $faker->numberBetween($min = 1, $max = 1000),
        'stock' => $faker->randomDigitNotNull,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Veterinaria\Ventas::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement($array = array ('Vacuna','AlimentoDog','AlimentoCat','AlimentoAve','Plato','Correa','Medicamento 1','Medicamento 2','Juguete para perro', 'juguete para gato', 'Alimento para peces', 'Venda', 'Desparasitante', 'Vitaminas')),
        'precio' => $faker->numberBetween($min = 1, $max = 1000),
        'stock' => $faker->randomDigitNotNull,
    ];
});