@extends('layouts.appHome')
@section('titulo', 'Animal')
@section('contenido')
@include('animales._menuAnimal')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DEL ANIMAL</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $animal->id }}</td>
        </tr>
        <tr>
          <td>Nombre:</td>
          <td>{{ $animal->nombre }}</td>
        </tr>
        <tr>
          <td>Especie:</td>
          <td>{{ $animal->especie }}</td>
        </tr>
        <tr>
          <td>Raza:</td>
          <td>{{ $animal->raza }}</td>
        </tr>
        <tr>
          <td>Genero:</td>
          <td>{{ $animal->genero }}</td>
        </tr>
        <tr>
          <td>Fecha de Nacimiento:</td>
          <td>{{ $animal->fechaNacimiento }}</td>
        </tr>
        <tr>
          <td>Dueño:</td>
          <td>
            <a href="{{ route('cliente_path',['cliente' =>$animal->cliente->id]) }}">
              {{ $animal->cliente->id }}: {{ $animal->cliente->nombre }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $animal->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>

    @endsection
