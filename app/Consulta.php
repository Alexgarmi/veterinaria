<?php

namespace Veterinaria;

use Veterinaria\Animal;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Consulta extends Model{

	use DatesTranslator;

	protected $table = 'consultas';

	protected $fillable = ['diagnostico', 'receta', 'observaciones', 'pendiente', 'id_animal', 'id_user'];
	//Veterinaria\Cliente::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'])

	// para identificar las columnas que sea necesario llenarlas
	protected $guarded = [];

	public function animal()
	{
		return $this->belongsTo('Veterinaria\Animal', 'id_animal');
	}

	public function user()
	{
		return $this->belongsTo('Veterinaria\User', 'id_user');
	}

//	{
//  	return $this->hasMany('Veterinaria\Venta', 'id_venta');
//	}

}