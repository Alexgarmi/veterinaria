<?php

use Illuminate\Database\Seeder;
use Veterinaria\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_employee = new Role();
      $role_employee->name = 'administrador';
      $role_employee->description = 'Administrador';
      $role_employee->save();

      $role_employee = new Role();
      $role_employee->name = 'veterinario';
      $role_employee->description = 'Veterinario';
      $role_employee->save();

      $role_employee = new Role();
      $role_employee->name = 'empleado';
      $role_employee->description = 'Empleado';
      $role_employee->save();

      $role_employee = new Role();
      $role_employee->name = 'almacen';
      $role_employee->description = 'Almacen';
      $role_employee->save();

      $role_employee = new Role();
      $role_employee->name = 'recepcion';
      $role_employee->description = 'Recepcion';
      $role_employee->save();

      $role_employee = new Role();
      $role_employee->name = 'caja';
      $role_employee->description = 'Caja';
      $role_employee->save();
    }
}
