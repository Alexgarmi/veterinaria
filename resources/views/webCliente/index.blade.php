@extends('webCliente.app')
@section('titulo', 'Cliente')
@section('contenido')
<br/><br/>
<div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4>
        Estimad@ cliente se Ud. Bienvenid@ <strong>{{ $cliente->nombre }}</strong>
      </h4>
      Tiene
      <strong>{{ count($cliente->animales) }}</strong>
      Mascotas registrados en nuetra Veterinaria.
    </div>
    <div class="panel-body">
      Estas son las Notificaciones e Historial de sus Mascotas:
      @foreach($cliente->animales as $animal)
      <a href="#{{ $animal->nombre }}" class="btn btn-link">
        {{ $animal->nombre }}
      </a>
      @endforeach
    </div>
  </div>
  <div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Notificación Vacunas!</h4>
    @foreach($cliente->animales as $animal)
    @foreach($animal->vacunas as $vacuna)
    @if(!$vacuna->aplicado && $vacuna->fechaAplicacion> new \Carbon\Carbon())
    <p>{{ $animal->nombre }} tiene una vacuna pendiente en fecha {{ $vacuna->fechaAplicacion }}</p>
    @endif
    @endforeach
    @endforeach
  </div>
  <div class="alert alert-dismissible alert-warning">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Notificación Internacion!</h4>
    @foreach($cliente->animales as $animal)
    @foreach($animal->internaciones as $internacion)
    @if($internacion->fechaSalida> new \Carbon\Carbon())
    <p>La internacion de {{ $animal->nombre }} por {{ $internacion->motivo }}, temina en fecha {{ $internacion->fechaSalida }}.</p>
    @endif
    @endforeach
    @endforeach
  </div>
  <div class="panel panel-primary">
    <div class="panel-heading" align="center">
      <h3 class="panel-title">Historial de sus Mascotas</h3>
    </div>
  </div>
  <br>
  @foreach($cliente->animales as $animal)
  <A name="{{ $animal->nombre }}"></A>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h3 class="panel-title">
        {{ $animal->nombre }}
      </h3>
    </div>
    @if(count($animal->consultas)>0)
    <div class="panel-body">
      <div class="panel panel-default">
        <div class="panel-heading" align="center">
          <h3 class="panel-title" align="center">CONSULTAS DE <strong>{{ $animal->nombre }}</strong> </h3>
        </div>
        <div class="panel-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No. ID</th>
                <th>Paciente</th>
                <th>Fecha / Hora</th>
                <th>Médico</th>
              </tr>
            </thead>
            <tbody>
              @foreach($animal->consultas as $consulta)
              <tr>
                <td>{{ $consulta->id }}</td>
                <td>{{ $consulta->animal->nombre }}</td>
                <td>{{ $consulta->created_at }}</td>
                <td>{{ $consulta->user->name }}</td>
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      @else
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading" align="center">
            <h3 class="panel-title" align="center">
              <span class="text-warning">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
              </span>
              <strong>{{ $animal->nombre }}</strong> No tiene Consultas registradas.
            </h3>
          </div>
        </div>
      </div>
      @endif
      @if(count($animal->internaciones)>0)
      <div class="panel panel-default">
        <div class="panel-heading" align="center">
          <h3 class="panel-title">INFORMACIÓN DE LA INTERNACIÓN DE <strong>{{ $animal->nombre }}</strong></h3>
        </div>
        <div class="panel-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No. ID</th>
                <th>Paciente</th>
                <th>Motivo</th>
                <th>Fecha de Ingreso</th>
                <th>Fecha de Salida</th>
              </tr>
            </thead>
            <tbody>
              @foreach($animal->internaciones as $internacion)
              <tr>
                <td>{{ $internacion->id }}</td>
                <td>{{ $internacion->animal->nombre }}</td>
                <td>{{ $internacion->motivo }}</td>
                <td>{{ $internacion->fechaIngreso }}</td>
                <td>{{ $internacion->fechaSalida }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @else
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading" align="center">
            <h3 class="panel-title" align="center">
              <span class="text-warning">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
              </span>
              <strong>{{ $animal->nombre }}</strong> No tiene Internaciones registradas.
            </h3>
          </div>
        </div>
      </div>
      @endif
      @if(count($animal->laboratorios)>0)
      <div class="panel panel-default">
        <div class="panel-heading" align="center">
          <h3 class="panel-title" align="center">LABORATORIOS DE <strong>{{ $animal->nombre }}</strong></h3>
        </div>
        <div class="panel-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No. ID</th>
                <th>Paciente</th>
                <th>Tipo Examen</th>
                <th>Muestra</th>
              </tr>
            </thead>
            <tbody>
              @foreach($animal->laboratorios as $laboratorio)
              <tr>
                <td>{{ $laboratorio->id }}</td>
                <td>{{ $laboratorio->animal->nombre }}</td>
                <td>{{ $laboratorio->tipoExamen }}</td>
                <td>{{ $laboratorio->muestra }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @else
      <div class="panel-body">
        <div class="panel panel-default">
          <div class="panel-heading" align="center">
            <h3 class="panel-title" align="center">
              <span class="text-warning">
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
              </span>
              <strong>{{ $animal->nombre }}</strong> No tiene Laboratorios registrados.
            </h3>
          </div>
        </div>
      </div>
      @endif
      @if(count($animal->vacunas)>0)
      <div class="panel panel-default">
        <div class="panel-heading" align="center">
          <h3 class="panel-title" align="center">VACUNAS DE <strong>{{ $animal->nombre }}</strong></h3>
        </div>
        <div class="panel-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No. ID</th>
                <th>Paciente</th>
                <th>Nombre Vacuna</th>
                <th>Fecha Aplicacion</th>
              </tr>
            </thead>
            <tbody>
              @foreach($animal->vacunas as $vacuna)
              <tr>
                <td>{{ $vacuna->id }}</td>
                <td>{{ $vacuna->animal->nombre }}</td>
                <td>{{ $vacuna->nombre }}</td>
                <td>{{ $vacuna->fechaAplicacion }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @else
        <div class="panel-body">
          <div class="panel panel-default">
            <div class="panel-heading" align="center">
              <h3 class="panel-title" align="center">
                <span class="text-warning">
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                </span>
                 <strong>{{ $animal->nombre }}</strong> No tiene Vacunas registradas.
              </h3>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  @endforeach
  </div>

<br/>
<br/>
<br/>
@endsection
