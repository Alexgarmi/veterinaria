@extends('layouts.appHome')
@section('titulo', 'Internaciones')
@section('contenido')
@include('internaciones._menu')

<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LISTADO DE INTERNACIONES</h3>
  </div>
  <div class="panel-body table-responsive">
  @include('layouts._messages')
  @include('layouts._errors')
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Paciente</th>
          <th>Fecha / Hora - Ingreso</th>
          <th>Fecha / Hora - Salida</th>
          <th>Médico</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($internaciones as $internacion)
        <tr>
          <td>{{ $internacion->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$internacion->animal->id]) }}">  
              {{ $internacion->animal->nombre }}
            </a>
          </td>
          <td>{{ $internacion->fechaIngreso }}</td>
          <td>{{ $internacion->fechaSalida }}</td>
          <td>{{ $internacion->user->name }}</td>
          <td>
            <a href="{{ route('internacion_path',['internacion' => $internacion->id]) }}" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Ver</a>
          </td>
         
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $internaciones->render() }}
  </div>
</div>
@endsection

