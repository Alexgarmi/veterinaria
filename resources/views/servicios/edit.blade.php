@extends('layouts.appHome')

@section('titulo', 'Editar Servicio')
@include('servicios._menu')
@section('contenido')

	
	@include('servicios._form',['servicio' => $servicio])


@endsection