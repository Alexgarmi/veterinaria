<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">CREAR / EDITAR INFORMACIÓN DEL USUARIO</h3>
  </div>
  <div class="panel-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @include('layouts._errors')
          <div class="panel panel-default">
            <div class="panel-heading">Ingresar Datos del Usuario - No. {{ $usuario->id }}:</div>
            <div class="panel-body">
              @if( $usuario->exists )
                <form action="{{ route('update_usuario_path',['usuario' =>$usuario->id]) }}" class="form-horizontal" method="POST" role="form">
                {{ method_field('PUT') }}
              @else
                <form action="{{ route('store_usuarios_path') }}" class="form-horizontal" method="POST" role="form">
              @endif
              {{ csrf_field() }}
              <fieldset>
                <div class="form-group">
                  <label class="col-lg-2 control-label" for="nombreUsuario">Nombre *</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-user" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" name="nombreUsuario" placeholder="Nombre del usuario" type="text" value="{{ $usuario->name or old('nombreUsuario') }}">
                      </input>
                    </div>
                  </div>
                </div>
            <div class="form-group">
              <label class="col-lg-2 control-label" for="emailUsuario">Correo electrónico *</label>
              <div class="col-lg-10">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                  <input class="form-control" name="emailUsuario" placeholder="Correo electrónico del usuario" type="text" value="{{ $usuario->email or old('emailUsuario') }}">
                </input>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label" for="passwordUsuario">Password</label>
            <div class="col-lg-10">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <input class="form-control" name="passwordUsuario" placeholder="Password del usuario" type="passwordUsuario" value="{{ old('passwordUsuario') }}">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-2 control-label" for="id_rol">Rol</label>
            <div class="col-lg-10">
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <select class="select form-control" id="id_rol" name="id_rol">
                  @foreach($roles as $role)
                    <option value="{{ $role->id }}" {{$usuario->roles->isNotEmpty() && $usuario->roles[0]->id == $role->id ? 'selected' : ''}}>
                      {{ $role->name }}
                    </option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
        @if($usuario->id == null)        
          <a href="{{ route('usuarios_path') }}" class="btn btn-default">Cancelar</a>
        @else
          <a href="{{ route('usuario_path',['usuario' =>$usuario->id]) }}" class="btn btn-default">Cancelar</a>
        @endif
          <button class="btn btn-primary" type="submit">Guardar Usuario</button>
        </div>
      </div>
    </fieldset>
  </form>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

