<?php

use Illuminate\Database\Seeder;
use Veterinaria\User;
use Veterinaria\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_administrador = Role::where('name', 'administrador')->first();
      $role_veterinario = Role::where('name', 'veterinario')->first();
      $role_empleado = Role::where('name', 'empleado')->first();
      $role_almacen = Role::where('name', 'almacen')->first();
      $role_recepcion = Role::where('name','recepcion')->first();

      $administrador = new User();
      $administrador->name = 'admin';
      $administrador->email = 'admin@example.com';
      $administrador->password = bcrypt('123');
      $administrador->save();
      $administrador->roles()->attach($role_administrador);

      $veterinario = new User();
      $veterinario->name = 'veterinario';
      $veterinario->email = 'veterinario@example.com';
      $veterinario->password = bcrypt('123');
      $veterinario->save();
      $veterinario->roles()->attach($role_veterinario);

      $empleado = new User();
      $empleado->name = 'empleado';
      $empleado->email = 'empleado@example.com';
      $empleado->password = bcrypt('123');
      $empleado->save();
      $empleado->roles()->attach($role_empleado);

      $almacen = new User();
      $almacen->name = 'almacen';
      $almacen->email = 'almacen@example.com';
      $almacen->password = bcrypt('123');
      $almacen->save();
      $almacen->roles()->attach($role_almacen);

      $recepcion = new User();
      $recepcion->name = 'recepcion';
      $recepcion->email = 'recepcion@example.com';
      $recepcion->password = bcrypt('123');
      $recepcion->save();
      $recepcion->roles()->attach($role_recepcion);

      $recepcion = new User();
      $recepcion->name = 'caja';
      $recepcion->email = 'caja@example.com';
      $recepcion->password = bcrypt('123');
      $recepcion->save();
      $recepcion->roles()->attach($role_recepcion);
    }
}
