<?php

namespace Veterinaria;

use Veterinaria\Animal;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Internacion extends Model{

	use DatesTranslator;

	protected $table = 'internaciones';

	protected $fillable = ['motivo', 'fechaIngreso', 'fechaSalida', 'pendiente', 'id_animal', 'id_user'];

	public function animal()
	{
		return $this->belongsTo('Veterinaria\Animal', 'id_animal');
	}

	public function user()
	{
		return $this->belongsTo('Veterinaria\User', 'id_user');
	}

}