@extends('layouts.appHome')

@section('titulo', 'Venta')
@section('contenido')
@include('ventas._menu')
@include('layouts._messages')
@include('layouts._errors')	

<blockquote>
	<h4>No. {{ $venta->id }}</h4>
	<h3>Cliente: {{ $venta->cliente->nombre }}</h3>
	<p>Fecha Pago: {{ $venta->fechaPago }}</p>
  	<small>Venta registrada hace {{ $venta->created_at->diffForHumans() }}</small>
  <div class="panel-body table-responsive">

	<table class="table table-striped table-hover " >
  <thead>
    <tr>
      <th>#</th>
      <th>Producto</th>
      <th with="100" align="right">Precio</th>
    </tr>
  </thead>
  <tbody>
  @foreach($productos as $producto)
    <tr>
      <td>{{ $producto->id }}</td>
      <td>{{ $producto->nombre }}</td>
      <td with="100" align="right">{{ $producto->precio }}</td>
    </tr>
 @endforeach
    <tr>
        <td colspan="2"><h4>Total</h4></td>
        <td with="100" align="right">{{$venta->total}}</td>
        <td></td>
    </tr>
  </tbody>
</table> 
</div>
</blockquote>

@endsection