@extends('layouts.appHome')
@section('titulo', 'Animales')
@section('contenido')
@include('animales._menu')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">LISTADO DE ANIMALES</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-hover ">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Raza / Especie</th>
          <th>Propietario</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($animales as $animal)      
        <tr>
          <td>{{ $animal->id }}</td>
          <td>
              <a href="{{ route('animal_path',['animal' =>$animal->id]) }}">
                {{ $animal->nombre }}
              </a>
          </td>
          <td>
             {{ $animal->raza }} / {{ $animal->especie }}
          </td>
          <td>
              <a href="{{ route('cliente_path',['cliente' =>$animal->cliente->id]) }}">
                {{ $animal->cliente->id }}: {{ $animal->cliente->nombre }}
              </a>
          </td>
          <td><a href="{{ route('animal_historial_path',['animal' =>$animal->id]) }}" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Historial</a></td>
          <td>
            <form action="{{ route('delete_animal_path',['animal'=>$animal->id]) }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-link btn-xs">
                <spam class="text-danger">
                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                  Eliminar
                </spam>
              </button>
            </form>
          </td>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div>{{ $animales->render() }}</div>
</div>
</div>
@endsection
