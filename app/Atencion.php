<?php

namespace Veterinaria;

use Illuminate\Database\Eloquent\Model;

class Atencion extends Model
{
    protected $table = 'atenciones';

    public function animal()
    {
        return $this->belongsTo('Veterinaria\Animal', 'id_animal');
    }

    public function servicio()
    {
        return $this->belongsTo('Veterinaria\Servicio', 'id_servicio');
    }
}
