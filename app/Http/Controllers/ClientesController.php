<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Cliente;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateClienteRequest;
use Veterinaria\Http\Requests\UpdateClienteRequest;

class ClientesController extends Controller
{
 //   public function _construct(){
//      $this->middleware('auth');
//        $this->middleware('admin');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::orderBy('id', 'desc')->paginate(4);

        return view('clientes.index')->with(['clientes' => $clientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cliente = new Cliente;

        return view('clientes.create')->with(['cliente' => $cliente]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClienteRequest $request)
    {
        
        $cliente = Cliente::create(
            $request->only('nombre', 'ci', 'email', 'telefono', 'direccion')
        );

        session()->flash('message', 'El cliente fue agregado a la Base de Datos!');


        //Veterinaria\Cliente::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'];

        return redirect()->route('clientes_path'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
       
        return view('clientes.show')->with(['cliente' => $cliente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        return view('clientes.edit')->with(['cliente' => $cliente]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClienteRequest $request, Cliente $cliente)
    {
        $cliente->update(
            $request->only('nombre', 'ci', 'email', 'telefono', 'direccion')
        );

        session()->flash('message', 'Se actualizó la información del cliente!');

        return redirect()->route('cliente_path',['cliente' => $cliente->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Cliente $cliente)
    {
        $cliente->delete();

        session()->flash('message', 'El cliente fue eliminado de la Base de Datos!');

        return redirect()->route('clientes_path');
    }
}
