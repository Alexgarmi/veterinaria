<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">CREAR NUEVA VENTA</h3>
  </div>
  <div class="panel-body">
    
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">
          Datos de la Venta:
        </div> 
        @include('layouts._messages')
        <div class="panel-body">
          <form action="{{ route('store_ventas_path') }}" class="form-horizontal" method="POST" role="form">
            {{ csrf_field() }}
            <fieldset>
              <div class="form-group">
                <input class="form-control" name="id" type="hidden" value="{{ $venta->id}}">
                  <label class="col-lg-2 control-label" for="id_cliente">
                    Cliente
                  </label>
                  <div class="col-lg-10">
                    <input class="form-control" disabled="" type="text" value="{{ $venta->cliente->nombre}}">
                      <input class="form-control" name="id_cliente" type="hidden" value="{{ $venta->cliente->id}}">
                        <input class="form-control" name="total" type="hidden" value="{{ $total}}">
                        </input>
                      </input>
                    </input>
                  </div>
                </input>
              </div>
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <a class="btn btn-warning" href="{{ route('select_producto_venta_path',['venta' => $venta->id]) }}">
                   <i class="fa fa-shopping-cart" aria-hidden="true"></i> Añadir Productos
                  </a>
                  <button class="btn btn-success" type="submit">
                    <i class="fa fa-money" aria-hidden="true"></i> Vender
                  </button>
                </div>
              </div>
            </fieldset>
          </form>
          <table class="table table-striped table-hover ">
            <thead>
              <tr>
                <th>
                  #
                </th>
                <th>
                  Producto
                </th>
                <th align="right">
                  Precio
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($atenciones as $atencion)
              <tr>
                <td>
                  {{ $atencion->id }}
                </td>
                <td>
                  {{ $atencion->servicio->nombreServicio }}
                </td>
                <td with="100" align="right">
                  {{ $atencion->servicio->precio }}
                </td>
              </tr>
              @endforeach
              @foreach($consultas as $consulta)
              <tr>
                <td>
                  {{ $consulta->id }}
                </td>
                <td>
                  Consulta
                </td>
                <td with="100" align="right">
                  50
                </td>
              </tr>
              @endforeach
              @foreach($internaciones as $internacion)
              <tr>
                <td>
                  {{ $internacion->id }}
                </td>
                <td>
                  Internacion: {{ $internacion->motivo }}
                </td>
                <td with="100" align="right">
                  100
                </td>
              </tr>
              @endforeach
              @foreach($vacunas as $vacuna)
              <tr>
                <td>
                  {{ $vacuna->id }}
                </td>
                <td>
                  {{ $vacuna->nombre }}
                </td>
                <td with="100" align="right">
                  10
                </td>
              </tr>
              @endforeach
              @foreach($productos as $producto)
              <tr>
                <td>
                  {{ $producto->id }}
                </td>
                <td>
                  {{ $producto->nombre }}
                </td>
                <td with="100" align="right">
                  {{ $producto->precio }}
                </td>
              </tr>
              @endforeach
              <tr>
                <td colspan="2">
                  <h4>
                    Total
                  </h4>
                </td>
                <td with="100" align="right">
                  {{$total}}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


  </div>
</div>



