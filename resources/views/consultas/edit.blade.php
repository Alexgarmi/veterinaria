@extends('layouts.appHome')
@section('titulo', 'Editar Consulta')
@section('contenido')
@section('tituloCentral', 'EDITAR CONSULTA')
@include('consultas._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('consultas._form',['consulta' => $consulta])
@endsection