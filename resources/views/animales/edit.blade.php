@extends('layouts.appHome')
@section('titulo', 'Editar Animal')
@section('contenido')
@section('tituloCentral', 'EDITAR DATOS DEL ANIMAL')
@include('animales._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('animales._form',['animal' => $animal, 'cliente' => $cliente])
@endsection
