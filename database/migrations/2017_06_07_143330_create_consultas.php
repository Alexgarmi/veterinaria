<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('diagnostico');
            $table->string('receta')->nullable();
            $table->string('observaciones')->nullable();
            $table->boolean('pendienteConsulta')->default(0);
            
            $table->string('motivoInternacion')->nullable();
            $table->date('fechaSalida')->nullable();
            $table->boolean('pendienteInternacion')->default(0);            
            $table->integer('id_animal')->unsigned();
            $table->integer('id_user')->unsigned();

            $table->timestamps();

            $table->foreign('id_animal')
                ->references('id')
                ->on('animales');
                
            $table->foreign('id_user')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
