@extends('layouts.appHome')
@section('titulo', 'Crear Animal')
@section('contenido')
@section('tituloCentral', 'CREAR NUEVO ANIMAL')
@include('animales._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('animales._form',['animal' => $animal, 'cliente' => $cliente])
@endsection
