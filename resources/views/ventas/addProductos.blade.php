@extends('layouts.appHome')

@section('titulo', 'Crear Venta')

@section('contenido')
@include('ventas._menu')


@include('layouts._errors')

	@include('ventas._form',['venta' => $venta, 'productos' => $productos, 'atenciones' => $atenciones,
            'consultas' => $consultas,
            'internaciones' => $internaciones,
            'vacunas' => $vacunas, 'total' => $total])

@endsection