<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratorios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratorios', function (Blueprint $table) {
            $table->increments('id');

            $table->string('muestra');
            $table->string('tipoExamen')->nullable();
            $table->string('resultado')->nullable();
            $table->binary('documentoDigital')->nullable();
            $table->integer('id_animal')->unsigned();
            $table->integer('id_user')->unsigned();

            $table->timestamps();

            $table->foreign('id_animal')
                ->references('id')
                ->on('animales');
                
            $table->foreign('id_user')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratorios');
    }
}
