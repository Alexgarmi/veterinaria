@extends('layouts.appHome')
@section('titulo', 'Crear Usuario')
@section('contenido')
@include('usuarios._menu')
@include('layouts._messages')

@include('usuarios._form', ['usario' => $usuario])
@endsection
