@extends('layouts.appHome')
@section('titulo', 'Usuario')
@section('contenido')
@include('usuarios._menuUsuario')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-info">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DEL USUARIO</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $usuario->id }}</td>
        </tr>
        <tr>
          <td>Nombre:</td>
          <td>{{ $usuario->name }}</td>
        </tr>
        <tr>
          <td>Correo:</td>
          <td>{{ $usuario->email }}</td>
        </tr>
        <tr>
          <td>Rol:</td>
          <td>{{ $usuario->roles[0]->name }}</td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $usuario->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>

    @endsection
