@extends('layouts.appHome')
@section('titulo', 'Usuarios')
@section('contenido')
@include('usuarios._menu')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">LISTADO DE USUARIOS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-hover ">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Correo electrónico</th>
          <th>Rol</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($usuarios as $usuario)      
        <tr>
          <td>{{ $usuario->id }}</td>
          <td>
              <a href="{{ route('usuario_path',['usuario' =>$usuario->id]) }}">
                {{ $usuario->name }}
              </a>
          </td>
          <td>
             {{ $usuario->email }}
          </td>
          <td>
             {{ $usuario->roles[0]->name }}
          </td>
          
          <td>
            <form action="{{ route('delete_usuario_path',['usuario'=>$usuario->id]) }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-link btn-xs">
                <spam class="text-danger">
                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                  Eliminar
                </spam>
              </button>
            </form>
          </td>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div>{{ $usuarios->render() }}</div>
</div>
</div>
@endsection
