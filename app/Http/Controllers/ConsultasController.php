<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Consulta;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateConsultaRequest;
use Veterinaria\Http\Requests\UpdateConsultaRequest;

class ConsultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $consultas = Consulta::orderBy('id', 'desc')->paginate(4);

        return view('consultas.index')->with(['consultas' => $consultas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Animal $animal)
    {
        $consulta = new Consulta;
        return view('consultas.create')->with(['consulta' => $consulta, 'animal' => $animal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateConsultaRequest $request)
    {
         $consulta = Consulta::create(
            $request->only('diagnostico', 'receta', 'observaciones', 'id_animal', 'id_user')
        );

        session()->flash('message', 'La consulta fue agregada a la Base de Datos!');


        //Veterinaria\Cliente::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'];

        return redirect()->route('consultas_path'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Consulta $consulta)
    {
        return view('consultas.show')->with(['consulta'=>$consulta]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Consulta $consulta)
    {
        return view('consultas.edit')->with(['consulta' => $consulta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
