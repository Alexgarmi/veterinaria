@extends('layouts.appHome')
@section('titulo', 'Registrar Internacion')
@section('contenido')
@include('laboratorios._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('laboratorios._form',['laboratorio' => $laboratorio, 'animal' => $animal])
@endsection