<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Producto;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateProductoRequest;
use Veterinaria\Http\Requests\UpdateProductoRequest;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Producto::orderBy('id', 'desc')->paginate(4);

        return view('productos.index')->with(['productos' => $productos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $producto = new Producto;

        return view('productos.create')->with(['producto' => $producto]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductoRequest $request)
    {
        
        $producto = Producto::create(
            $request->only('nombre', 'precio', 'stock')
        );

        session()->flash('message', 'El producto fue agregado al sistema!');


        //Veterinaria\Cliente::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'];

        return redirect()->route('productos_path'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        return view('productos.show')->with(['producto' => $producto]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        return view('productos.edit')->with(['producto' => $producto]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductoRequest $request, Producto $producto)
    {
        $producto->update(
            $request->only('nombre', 'precio', 'stock')
        );

        session()->flash('message', 'Se actualizó la información del producto!');

        return redirect()->route('producto_path',['producto' => $producto->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Producto $producto)
    {
        $producto->delete();

        session()->flash('message', 'Producto eliminado del sistema!');

        return redirect()->route('productos_path');
    }
}
