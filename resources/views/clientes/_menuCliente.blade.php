<ul class="nav nav-tabs">

  <li class="{{ Request::path() == 'clientes' ? 'active' : '' }}">
    <a href="{{ route('create_cliente_path') }}">
    <span class="text-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo Cliente</a></span>
  </li>

  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    <a href="{{ route('clientes_path') }}">
    <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i> Listar Clientes</a>
  </li>

  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }} ">
    <a href="{{ route('edit_cliente_path', ['cliente' =>$cliente->id]) }}">
    <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Cliente</a></span>
  </li>

  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
  @if(count($cliente->animales)==0)
    <form action="{{ route('delete_cliente_path',['cliente'=>$cliente->id]) }}" method="POST">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
			<button type="submit" class="btn btn-link text-danger">
        <span class="text-primary"><i class="fa fa-trash-o" aria-hidden="true"></i>  Eliminar Cliente</spam>
      </button>
    </form>
  @else
    <button class="btn btn-link disabled"><spam class="text-danger"><i class="fa fa-trash-o" aria-hidden="true"></i>  Eliminar Cliente</spam></button>
  @endif
  </li>
 
 </ul>


