@extends('layouts.appHome')
@section('contenido')
@include('layouts._messages')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <img src="{{ asset('img/veterinaria-home.png') }}" class="img-responsive center-block">
        </div>
    </div>
</div>
@endsection
