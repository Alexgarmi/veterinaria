@extends('layouts.appHome')
@section('titulo', 'Editar Cliente')
@section('contenido')
@include('clientes._menuEdit')
@include('layouts._messages')
@include('layouts._errors')
@include('clientes._form',['cliente' => $cliente])
@endsection
