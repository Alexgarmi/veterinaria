<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'internaciones' ? 'active' : '' }}">
    <a href="{{ route('internaciones_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Internaciones</span>
    </a>
  </li>
@if(Auth::user()->hasAnyRole(['administrador','veterinario']))  
  <li class="{{ Request::path() == 'internaciones/'.$internacion->id.'/edit' ? 'active' : '' }}">
    <a href="{{ route('edit_internacion_path', ['internacion' =>$internacion->id]) }}">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Internación</span>
    </a>
  </li>
@endif
</ul>
