@extends('layouts.appHome')

@section('titulo', 'Crear Servicio')

@section('contenido')
@include('servicios._menu')
@include('layouts._messages')
@include('layouts._errors')
	
	@include('servicios._form',['servicio' => $servicio])

@endsection