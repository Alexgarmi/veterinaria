<?php

namespace Veterinaria;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';

    protected $fillable = ['nombreServicio','precio'];

    public function atenciones()
	{
    	return $this->hasMany('Veterinaria\Atencion', 'id_atencion');
	}
}
