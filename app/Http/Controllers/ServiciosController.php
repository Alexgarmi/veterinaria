<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Servicio;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateServicioRequest;
use Veterinaria\Http\Requests\UpdateServicioRequest;

class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::orderBy('id', 'desc')->paginate(4);

        return view('servicios.index')->with(['servicios' => $servicios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicio = new Servicio;

        return view('servicios.create')->with(['servicio' => $servicio]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateServicioRequest $request)
    {
        
        $servicio = Servicio::create(
            $request->only('nombreServicio', 'precio')
        );

        session()->flash('message', 'El servicio fue agregado a la Base de Datos!');


        //Veterinaria\Servicio::create(['nombre'=>'Luis', 'telefono'=>'71111111', 'direccion'=>'Av. Heroinas No 100'];

        return redirect()->route('servicios_path'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_servicio
     * @return \Illuminate\Http\Response
     */
    public function edit(Servicio $servicio)
    {
        return view('servicios.edit')->with(['servicio' => $servicio]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServicioRequest $request, Servicio $servicio)
    {
        $servicio->update(
            $request->only('nombreServicio', 'precio')
        );

        session()->flash('message', 'Se actualizó la información del servicio!');

        return redirect()->route('servicio_path',['servicio' => $servicio->id]);
    }
}
