<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre')->default('Sin Nombre');
            $table->string('especie')->nullable();
            $table->string('raza')->nullable();
            $table->string('genero')->nullable();
            $table->date('fechaNacimiento')->nullable();
            $table->integer('id_cliente')->unsigned();

            $table->timestamps();

            $table->foreign('id_cliente')
                ->references('id')
                ->on('clientes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animales');
    }
}
