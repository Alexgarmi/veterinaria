<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
    <i class="fa fa-user-o" aria-hidden="true"></i> {{ Auth::user()->name }}
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu" role="menu">
  @if(Auth::user()->hasAnyRole(['administrador']))
    <li>
      <a href="{{ route('usuarios_path') }}" "> 
      <i class="fa fa-users" aria-hidden="true"></i> Usuarios</a>
    </li>
  @endif
    <li>
      <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
      <i class="fa fa-times-circle" aria-hidden="true"></i> Salir</a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
    </li>
    <li> <hr> </li>
  </ul>
</li>
