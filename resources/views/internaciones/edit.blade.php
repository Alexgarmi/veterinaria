@extends('layouts.appHome')
@section('titulo', 'Editar Internacion')
@section('contenido')
@section('tituloCentral', 'EDITAR INTERNACION')
@include('internaciones._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('internaciones._form',['internacion' => $internacion])
@endsection