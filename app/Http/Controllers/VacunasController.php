<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Vacuna;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateVacunaRequest;

class VacunasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacunas = Vacuna::orderBy('id', 'desc')->paginate(4);

        return view('vacunas.index')->with(['vacunas' => $vacunas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animales = Animal::all();

        return view('vacunas.select')->with(['animales' => $animales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFor(Animal $animal)
    {
        $vacuna = new Vacuna;
        $vacuna->id_animal = $animal->id;

        return view('vacunas.create')->with(['vacuna' => $vacuna])->with(['animal' => $animal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vacuna = new Vacuna;
        $vacuna->nombre = $request->get('nombre');
        $vacuna->fechaAplicacion = $request->get('fechaAplicacion');
        $vacuna->aplicado = $request->get('aplicado');
        $vacuna->pendiente = true;
        $vacuna->id_animal = $request->get('id_animal');
        $vacuna->id_user = $request->user()->id;

        $vacuna->save();

        session()->flash('message', 'La vacuna fue agregado a la Base de Datos!');

        return redirect()->route('animal_path', ['animal' => $vacuna->id_animal]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Vacuna $vacuna)
    {
        return view('vacunas.show')->with(['vacuna' => $vacuna]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Vacuna $vacuna)
    {
        return view('vacunas.edit')->with(['vacuna' => $vacuna]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateVacunaRequest $request, Vacuna $vacuna)
    {
        $vacuna->update(
            $request->only('nombre', 'fechaAplicacion', 'aplicado')
        );

        session()->flash('message', 'Se actualizó la información de la vacuna!');

        return redirect()->route('vacuna_path',['vacuna' => $vacuna->id]);
    }
}
