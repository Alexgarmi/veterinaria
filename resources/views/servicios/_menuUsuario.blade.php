<ul class="nav nav-tabs">
  
 @if(Auth::user()->hasAnyRole(['administrador']))
  <li class="{{ Request::path() == 'atenciones/create' ? 'active' : '' }}">
  	<a href="{{ route('create_atencion_path')}}">
    <span class="text-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nueva Atencion</span>
    </a>
  	</li>
@endif
  
  <li class="{{ Request::path() == 'atenciones' ? 'active' : '' }}">
  	<a href="{{ route('atenciones_path')}}">
    <span class="text-primary"><i class="fa fa-th-list" aria-hidden="true"></i> Historial Atenciones</span>
    </a>
  </li>
  
  <li class="{{ Request::path() == 'servicios' ? 'active' : '' }}">
  	<a href="{{ route('servicios_path')}}">
    <span class="text-primary"><span class="text-primary"> Servicios</span>
    </a>
  </li>

  
 @if(Auth::user()->hasAnyRole(['administrador']))
  <li class="{{ Request::path() == 'servicios/create' ? 'active' : '' }}">
  	<a href="{{ route('create_servicio_path')}}">
    <span class="text-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Servicio</span>
    </a>
  </li>
@endif

</ul>
