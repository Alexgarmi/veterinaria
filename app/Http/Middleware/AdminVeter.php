<?php

namespace Veterinaria\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Session;
use Closure;

class AdminVeter
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->auth->user()->hasAnyRole(['administrador','veterinario'])){
            session()->flash('message-error', 'Intentó acceder a una sección sin tener autorización!');
            return redirect()->to('/home');
        }
        return $next($request);
    }
}
