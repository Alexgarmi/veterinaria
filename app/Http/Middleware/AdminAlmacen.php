<?php

namespace Veterinaria\Http\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Session;
use Closure;


class AdminAlmacen
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->auth->user()->hasAnyRole(['administrador','almacen'])){
            session()->flash('message-error', 'No tiene privilegios para acceder a esta sección');
            return redirect()->to('/home');
        }
        return $next($request);
    }
}