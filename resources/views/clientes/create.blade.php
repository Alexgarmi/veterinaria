@extends('layouts.appHome')
@section('titulo', 'Crear Cliente')
@section('contenido')
@include('clientes._menu')
@include('layouts._messages')

@include('clientes._form', ['cliente' => $cliente])
@endsection
