<ul class="nav nav-tabs">
<!--
  <li class="{ { Request::path() == 'animales/create' ? 'active' : '' }}">
    <a href="{ { route('create_animal_path') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Animal</a>
  </li>
-->
  <li class="{{ Request::path() == 'animales' ? 'active' : '' }}">
   <a href="{{ route('animales_path') }}">
   <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i> Listar Animales</span></a>
  </li>
<!--  
  <li class="disabled text-info">
   <a href="">
   <span class="text-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Animal</span></a>
  </li> 
    <li class="disabled text-info">
   <a href="">
    <span class="text-primary"><i class="fa fa-folder-o" aria-hidden="true"></i> Historial</span></a>
  </li> 
-->  
</ul>
