@extends('layouts.appHome')
@section('titulo', 'Vacunas')
@section('contenido')
@include('vacunas._menu')

<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LISTADO DE VACUNAS</h3>
  </div>
  <div class="panel-body table-responsive">
  @include('layouts._messages')
  @include('layouts._errors')
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Paciente</th>
          <th>Nombre Vacuna</th>
          <th>Fecha Aplicacion</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($vacunas as $vacuna)
        <tr>
          <td>{{ $vacuna->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$vacuna->animal->id]) }}">  
              {{ $vacuna->animal->nombre }}
            </a>
          </td>
          <td>{{ $vacuna->nombre }}</td>
          <td>{{ $vacuna->fechaAplicacion }}</td>
          <td>
            <a href="{{ route('vacuna_path',['vacuna' => $vacuna->id]) }}" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Ver</a>
          </td>
          <td>
          </td>
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $vacunas->render() }}
  </div>
</div>
@endsection

