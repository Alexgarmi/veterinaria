<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">AGREGAR NUEVO PRODUCTO</h3>
  </div>
  <div class="panel-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">Ingresar Datos del Producto:</div>
            <div class="panel-body">
              @if( $producto->
              exists )
              <form class="form-horizontal" action="{{ route('update_producto_path',['producto' =>$producto->id]) }}" method="POST" role="form">
                {{ method_field('PUT') }}
                @else
                <form class="form-horizontal" action="{{ route('store_productos_path') }}" method="POST" role="form">
                  @endif
                  {{ csrf_field() }}
                  <fieldset>


                <div class="form-group ">
                  <label for="Nombre" class="col-lg-2 control-label">Nombre</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-product-hunt" aria-hidden="true"></i>
                      </div>
                      <input type="text" class="form-control" name="nombre" value="{{ $producto->nombre or old('nombre') }}" placeholder="Nombre del producto">
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                    <label for="inputPassword" class="col-lg-2 control-label">Precio</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-money" aria-hidden="true"></i>
                      </div>
                      <input type="text" class="form-control" name="precio" value="{{ $producto->precio or old('precio') }}" placeholder="Precio del producto">
                    </div>
                  </div>
                </div>


               <div class="form-group ">
                   <label for="direccion" class="col-lg-2 control-label">Stock</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-cubes" aria-hidden="true"></i>
                      </div>
                     <input type="number" class="form-control" name="stock" value="{{ $producto->stock or old('stock') }}" placeholder="Stock del producto">
                    </div>
                  </div>
                </div>

                    
 
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                        @if($producto->
                        exists)
                        <a href="{{ route('producto_path',['producto' =>$producto->id]) }}" class="btn btn-default">
                          Cancelar
                        </a>
                        @else
                        <a href="{{ route('productos_path') }}" class="btn btn-default">Cancelar</a>
                        @endif
                        <button type="submit" class="btn btn-primary">Guardar Producto</button>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
