@extends('layouts.appHome')
@section('titulo', 'Consultas')
@section('contenido')
@include('consultas._menuEditar')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DE LA CONSULTA</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $consulta->id }}</td>
        </tr>
        <tr>
          <td>Diagnostico:</td>
          <td>{{ $consulta->diagnostico }}</td>
        </tr>
        <tr>
          <td>Observaciones:</td>
          <td>{{ $consulta->observaciones }}</td>
        </tr>
        <tr>
          <td>Receta:</td>
          <td>{{ $consulta->receta }}</td>
        </tr>
        <tr>
          <td>Animal:</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$consulta->animal->id]) }}">
              {{ $consulta->animal->id }}: {{ $consulta->animal->nombre }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $consulta->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>
@endsection

