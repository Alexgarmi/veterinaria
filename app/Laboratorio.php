<?php

namespace Veterinaria;

use Veterinaria\Animal;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model{

	use DatesTranslator;

	protected $table = 'laboratorios';

	protected $fillable = ['muestra', 'tipoExamen', 'resultado', 'documentoDigital', 'id_animal', 'id_user'];

	public function animal()
	{
		return $this->belongsTo('Veterinaria\Animal', 'id_animal');
	}

	public function user()
	{
		return $this->belongsTo('Veterinaria\User', 'id_user');
	}

}