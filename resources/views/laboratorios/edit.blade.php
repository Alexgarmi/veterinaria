@extends('layouts.appHome')
@section('titulo', 'Editar Internacion')
@section('contenido')
@section('tituloCentral', 'EDITAR INTERNACION')
@include('laboratorios._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('laboratorios._form',['laboratorio' => $laboratorio])
@endsection