@extends('layouts.appHome')

@section('titulo', 'Atenciones')

@section('contenido')
@include('atenciones._menu')
@include('layouts._messages')
@include('layouts._errors')

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">HISTORIAL DE ATENCIONES</h3>
  </div>
  <div class="panel-body">
    
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Nombre del Animal</th>
      <th>Servicio Recibido</th>
      <th>Pagado</th>
      <th>Fecha de Atencion</th>
      <th></th>
    </tr>
  </thead>

  <tbody>
  @foreach($atenciones as $atencion)
    <tr>
      <td>{{ $atencion->id }}</td>
      <td>
        <a href="{{ route('animal_path',['animal' => $atencion->id_animal]) }}"> {{ $atencion->animal->nombre }}</a>
      </td>
      <td>{{ $atencion->servicio->nombreServicio }}</td>
      <td>{{ $atencion->pendiente? 'No' : 'Si' }}</td>
      <td>{{ $atencion->created_at }}</td>
    </tr>
 @endforeach
  </tbody>
</table> 

    
  </div>
</div>




{{ $atenciones->render() }}

@endsection