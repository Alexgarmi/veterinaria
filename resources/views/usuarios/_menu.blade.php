<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'usuarios/create' ? 'active' : '' }}">
    <a href="{{ route('create_usuario_path') }}">
      <span class="text-primary"><i class="fa fa-user-plus" aria-hidden="true"></i>
      Nuevo Usuario</span>
    </a>
  </li>
  <li class="{{ Request::path() == 'usuarios' ? 'active' : '' }}">
    <a href="{{ route('usuarios_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Usuario</span>
    </a>
  </li>
  <!--
  <li class="disabled text-info">
    <a href="">
     <span class="text-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Usuario</span>
    </a>
  </li>
  -->
</ul>

 