<ul class="nav nav-tabs">

  <li class="{{ Request::path() == 'usuarios' ? 'active' : '' }}">
    <a href="{{ route('create_usuario_path') }}">
    <span class="text-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo Usuario</a></span>
  </li>

  <li class="{{ Request::path() == 'usuarios/create' ? 'active' : '' }}">
    <a href="{{ route('usuarios_path') }}">
    <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i> Listar Usuarios</a></span>
  </li>

  <li class="{{ Request::path() == 'usuarios/create' ? 'active' : '' }} ">
    <a href="{{ route('edit_usuario_path', ['usuario' =>$usuario->id]) }}">
    <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Usuario</a></span>
  </li>
 
 </ul>


