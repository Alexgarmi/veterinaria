<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Laboratorio;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateLaboratorioRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LaboratoriosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laboratorios = Laboratorio::orderBy('id', 'desc')->paginate(4);

        return view('laboratorios.index')->with(['laboratorios' => $laboratorios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animales = Animal::all();

        return view('laboratorios.select')->with(['animales' => $animales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFor(Animal $animal)
    {
        $laboratorio = new Laboratorio;
        $laboratorio->id_animal = $animal->id;

        return view('laboratorios.create')->with(['laboratorio' => $laboratorio])->with(['animal' => $animal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $laboratorio = new Laboratorio;
        $laboratorio->muestra = $request->get('muestra');
        $laboratorio->tipoExamen = $request->get('tipoExamen');
        $laboratorio->resultado = $request->get('resultado');

        if ($request->hasFile('documentoDigital')){
            $file = $request->file('documentoDigital');
            $imagedata = file_get_contents($file->getRealPath());
            $base64 = base64_encode($imagedata);
            $laboratorio->documentoDigital = $base64;
        }

        $laboratorio->id_animal = $request->get('id_animal');
        $laboratorio->id_user = $request->user()->id;

        $laboratorio->save();

        session()->flash('message', 'La laboratorio fue agregado a la Base de Datos!');

        return redirect()->route('animal_path', ['animal' => $laboratorio->id_animal]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Laboratorio $laboratorio)
    {
        return view('laboratorios.show')->with(['laboratorio' => $laboratorio]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Laboratorio $laboratorio)
    {
        return view('laboratorios.edit')->with(['laboratorio' => $laboratorio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateLaboratorioRequest $request, Laboratorio $laboratorio)
    {
        $laboratorio->update(
            $request->only('muestra', 'tipoExamen', 'resultado')
        );

        if ($request->hasFile('documentoDigital')){
            $file = $request->file('documentoDigital');
            $imagedata = file_get_contents($file->getRealPath());
            $base64 = base64_encode($imagedata);
            $laboratorio->documentoDigital = $base64;
            $laboratorio->save();
        }

        session()->flash('message', 'Se actualizó la información de la laboratorio!');

        return redirect()->route('laboratorio_path',['laboratorio' => $laboratorio->id]);
    }
}
