<?php
 use Veterinaria\Laboratorio;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('index_path')->get('/', 'IndexController@index');

Route::name('webCliente_index_path')->post('/webCliente', 'IndexController@indexWeb');
Route::name('webCliente_path')->get('/webCliente/login', 'IndexController@login');


Auth::routes();



Route::group(['middleware' => 'auth'], function(){

Route::get('/home', 'HomeController@index');

//****** VISTAS CLIENTES ********//	
Route::name('clientes_path')->middleware('recepcion')->get('/clientes', 'ClientesController@index');
Route::name('create_cliente_path')->middleware('recepcion')->get('/clientes/create', 'ClientesController@create');
Route::name('store_clientes_path')->middleware('recepcion')->post('/clientes', 'ClientesController@store');
Route::name('cliente_path')->middleware('recepcion')->get('/clientes/{cliente}', 'ClientesController@show');
Route::name('edit_cliente_path')->middleware('recepcion')->get('/clientes/{cliente}/edit', 'ClientesController@edit');
Route::name('update_cliente_path')->middleware('recepcion')->put('/clientes/{cliente}', 'ClientesController@update');
Route::name('delete_cliente_path')->middleware('recepcion')->delete('/clientes/{cliente}', 'ClientesController@delete');

//****** VISTAS ANIMALES ********//	
Route::name('animales_path')->middleware('veterinario')->get('/animales', 'AnimalesController@index');
Route::name('create_animal_path')->middleware('veterinario')->get('/animales/create/{cliente}', 'AnimalesController@create');
Route::name('store_animales_path')->middleware('veterinario')->post('/animales', 'AnimalesController@store');
Route::name('animal_path')->middleware('veterinario')->get('/animales/{animal}', 'AnimalesController@show');
Route::name('edit_animal_path')->middleware('veterinario')->get('/animales/{animal}/edit', 'AnimalesController@edit');
Route::name('update_animal_path')->middleware('veterinario')->put('/animales/{animal}', 'AnimalesController@update');
Route::name('delete_animal_path')->middleware('veterinario')->delete('/animales/{animal}', 'AnimalesController@delete');
Route::name('animal_historial_path')->middleware('veterinario')->get('/animales/historial/{animal}', 'AnimalesController@historial');

Route::name('delete_animal_from_cliente_path')->middleware('veterinario')->delete('/animales/{animal}/{cliente}', 'AnimalesController@deleteFromCliente');
Route::name('create_animal_to_cliente_path')->middleware('veterinario')->get('/animales/create/{cliente}', 'AnimalesController@createWithCliente');

//****** VISTAS CONSULTAS ********//	
Route::name('consultas_path')->middleware('veterinario')->get('/consultas', 'ConsultasController@index');
Route::name('create_consulta_path')->middleware('adminVeter')->get('/consultas/create/{animal}', 'ConsultasController@create');
Route::name('store_consultas_path')->middleware('adminVeter')->post('/consultas', 'ConsultasController@store');
Route::name('consulta_path')->middleware('veterinario')->get('/consultas/{consulta}', 'ConsultasController@show');
Route::name('edit_consulta_path')->middleware('adminVeter')->get('/consultas/{consulta}/edit', 'ConsultasController@edit');
Route::name('update_consulta_path')->middleware('adminVeter')->put('/consultas/{consulta}', 'ConsultasController@update');

//****** VISTAS INTERNACIONES ****//
Route::name('internaciones_path')->middleware('veterinario')->get('/internaciones', 'InternacionesController@index');
Route::name('create_internacion_path')->middleware('adminVeter')->get('/internaciones/create/{animal}', 'InternacionesController@createFor');
Route::name('store_internaciones_path')->middleware('adminVeter')->post('/internaciones', 'InternacionesController@store');
Route::name('internacion_path')->middleware('veterinario')->get('/internaciones/{internacion}', 'InternacionesController@show');
Route::name('edit_internacion_path')->middleware('adminVeter')->get('/internaciones/{internacion}/edit', 'InternacionesController@edit');
Route::name('update_internacion_path')->middleware('adminVeter')->put('/internaciones/{internacion}', 'InternacionesController@update');

//****** VISTAS VACUNAS ****//
Route::name('vacunas_path')->middleware('veterinario')->get('/vacunas', 'VacunasController@index');
Route::name('create_vacuna_animal_path')->middleware('adminVeter')->get('/vacunas/create/{animal}', 'VacunasController@createFor');
Route::name('store_vacunas_path')->middleware('adminVeter')->post('/vacunas', 'VacunasController@store');
Route::name('vacuna_path')->middleware('veterinario')->get('/vacunas/{vacuna}', 'VacunasController@show');
Route::name('edit_vacuna_path')->middleware('adminVeter')->get('/vacunas/{vacuna}/edit', 'VacunasController@edit');
Route::name('update_vacuna_path')->middleware('adminVeter')->put('/vacunas/{vacuna}', 'VacunasController@update');

//****** VISTAS LABORATORIOS ****//
Route::name('laboratorios_path')->middleware('veterinario')->get('/laboratorios', 'LaboratoriosController@index');
Route::name('create_laboratorio_animal_path')->middleware('adminVeter')->get('/laboratorios/create/{animal}', 'LaboratoriosController@createFor');
Route::name('store_laboratorios_path')->middleware('adminVeter')->post('/laboratorios', 'LaboratoriosController@store');
Route::name('laboratorio_path')->middleware('veterinario')->get('/laboratorios/{laboratorio}', 'LaboratoriosController@show');
Route::name('edit_laboratorio_path')->middleware('adminVeter')->get('/laboratorios/{laboratorio}/edit', 'LaboratoriosController@edit');
Route::name('update_laboratorio_path')->middleware('adminVeter')->put('/laboratorios/{laboratorio}', 'LaboratoriosController@update');

//****** VISTAS PRODUCTOS ********//	
Route::name('productos_path')->middleware('almacen')->get('/productos', 'ProductosController@index');
Route::name('create_producto_path')->middleware('adminAlmacen')->get('/productos/create', 'ProductosController@create');
Route::name('store_productos_path')->middleware('adminAlmacen')->post('/productos', 'ProductosController@store');
Route::name('update_producto_path')->middleware('adminAlmacen')->put('/productos/{producto}', 'ProductosController@update');
Route::name('producto_path')->middleware('almacen')->get('/productos/{producto}', 'ProductosController@show');
Route::name('edit_producto_path')->middleware('adminAlmacen')->get('/productos/{producto}/edit', 'ProductosController@edit');
Route::name('delete_producto_path')->middleware('adminAlmacen')->delete('/productos/{producto}', 'ProductosController@delete');	

//****** VISTAS VENTAS ********//	
Route::name('ventas_path')->middleware('caja')->get('/ventas', 'VentasController@index');
Route::name('create_venta_path')->middleware('adminCaja')->get('/ventas/create', 'VentasController@create');

Route::name('add_producto_save_venta_path')->middleware('adminCaja')->post('/ventas/addProducto/', 'VentasController@addProductoSaveVenta');
Route::name('add_producto_venta_path')->middleware('adminCaja')->get('/ventas/addProducto/{venta}', 'VentasController@addProducto');
Route::name('select_producto_venta_path')->middleware('adminCaja')->get('/ventas/selectProducto/{venta}', 'VentasController@selectProducto');
Route::name('store_producto_path')->middleware('adminCaja')->post('/ventas/venta/{venta}/producto/{producto}', 'VentasController@storeProducto');

Route::name('store_ventas_path')->middleware('adminCaja')->post('/ventasstore', 'VentasController@store');
Route::name('venta_path')->middleware('caja')->get('/ventas/{venta}', 'VentasController@show');

//****** VISTAS SERVICIOS ********//	
Route::name('servicios_path')->get('/servicios', 'ServiciosController@index');
Route::name('create_servicio_path')->get('/servicios/create', 'ServiciosController@create');
Route::name('store_servicios_path')->post('/servicios', 'ServiciosController@store');
Route::name('update_servicio_path')->put('/servicios/{servicio}', 'ServiciosController@update');
Route::name('edit_servicio_path')->get('/servicios/{servicio}/edit', 'ServiciosController@edit');

//****** VISTAS ATENCIONES ********//	
Route::name('atenciones_path')->get('/atenciones', 'AtencionesController@index');
Route::name('create_atencion_path')->get('/atenciones/create', 'AtencionesController@create');
Route::name('store_atenciones_path')->post('/atenciones', 'AtencionesController@store');

//****** VISTAS USUARIOS ********//	
Route::name('usuarios_path')->get('/usuarios', 'UsuariosController@index');
Route::name('create_usuario_path')->get('/usuarios/create', 'UsuariosController@create');
Route::name('store_usuarios_path')->post('/usuarios', 'UsuariosController@store');
Route::name('update_usuario_path')->put('/usuarios/{usuario}', 'UsuariosController@update');
Route::name('usuario_path')->get('/usuarios/{usuario}', 'UsuariosController@show');
Route::name('edit_usuario_path')->get('/usuarios/{usuario}/edit', 'UsuariosController@edit');
Route::name('delete_usuario_path')->delete('/usuarios/{usuario}', 'UsuariosController@delete');

//****** VISTAS REPORTES ********//
Route::name('reporte_dias_path')->middleware('caja')->get('/reportes/dia', 'ReportesController@dia');
Route::name('reporte_mes_path')->middleware('caja')->get('/reportes/mes', 'ReportesController@mes');
Route::name('reporte_anyo_path')->middleware('caja')->get('/reportes/anyo', 'ReportesController@anyo');

Route::get('laboratorio/{id}/image', function($id)
{
   $laboratorio = Laboratorio::find($id);
   if($laboratorio->documentoDigital != null) {
    $response = Response::make($laboratorio->documentoDigital, 200);
    $response->header('Content-Type', 'image/jpeg');
   }
   return $response;
});

});