<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">CREAR/EDITAR ANIMAL</h3>
  </div>
  <div class="panel-body">
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Ingresar Datos del Animal - No. {{ $animal->id }}:</div>
        <div class="panel-body">
          @if( $animal-> exists )
          <form action="{{ route('update_animal_path',['animal' =>$animal->id]) }}" class="form-horizontal" method="POST" role="form">
            {{ method_field('PUT') }}
            @else
            <form action="{{ route('store_animales_path') }}" class="form-horizontal" method="POST" role="form">
              @endif
              {{ csrf_field() }}
              <fieldset>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="nombre">Nombre</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="nombre" name="nombre" placeholder="Nombre del animal" type="text" value="{{ $animal->nombre or old('nombre') }}"/>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="especie">Especie</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i aria-hidden="true" class="fa fa-github aria-hidden="true""></i>
                      </div>
                      <input class="form-control" id="especie" name="especie" placeholder="Especie" type="text" value="{{ $animal->especie or old('especie') }}"/>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2 control-label" for="raza">Raza</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i aria-hidden="true" class="fa fa-linux aria-hidden="true""></i>
                      </div>
                      <input class="form-control" id="raza" name="raza" placeholder="Raza" type="text" value="{{ $animal->raza or old('raza') }}"/>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="genero">Genero</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i aria-hidden="true" class="fa fa-venus-mars aria-hidden="true""></i>
                      </div>
                      <select class="select form-control" id="genero" name="genero" placeholder="Genero">
                        <option value="{{ $animal->genero or old('genero') }}">
                          {{ $animal->genero }}
                        </option>
                        <option value="Hembra">Hembra</option>
                        <option value="Macho">Macho</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="fechaNacimiento">Nacimiento</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i aria-hidden="true" class="fa fa-birthday-cake aria-hidden="true""></i>
                      </div>
                      <input class="form-control" id="fechaNacimiento" name="fechaNacimiento" placeholder="AAAA-MM-DD" type="date" value="{{ $animal->fechaNacimiento or old('fechaNacimiento') }}"/>
                    </div>
                  </div>
                </div>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="id_cliente">Nombre del Propietario </label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i aria-hidden="true" class="fa fa-user aria-hidden="true""></i>
                      </div>
                      @if($cliente)
                        <input class="form-control" id="cliente" name="cliente" placeholder="Cliente" type="text"  readonly="readonly" value="{{ $cliente->nombre }}"/>
                        <input type="hidden" name="id_cliente" value="{{ $cliente->id or old('id_cliente') }}"/>
                      @else
                      <select class="select form-control" id="id_cliente" name="id_cliente" value="{{ $animal->id_cliente or old('id_cliente') }}">
                        <option value="10">10</option>
                        <option value="11">11</option>
                      </select>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    @if($animal->exists)
                      <a href="{{ route('animal_path',['animal' =>$animal->id]) }}" class="btn btn-default">Cancelar</a>
                    @else
                      <a href="{{ route('cliente_path',['cliente' =>$cliente->id]) }}" class="btn btn-default">Cancelar</a>
                    @endif
                    <button class="btn btn-primary" type="submit">Guardar Animal</button>
                  </div>
                </div>
              </fieldset>
            </form>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>
</div>




