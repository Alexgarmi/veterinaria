<?php

namespace Veterinaria;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $fillable = ['idItem','tipoProducto'];
}
