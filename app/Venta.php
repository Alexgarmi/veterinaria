<?php

namespace Veterinaria;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{

    protected $fillable = ['fechaPago'];

    public function cliente()
    {
        return $this->belongsTo('Veterinaria\Cliente', 'id_cliente');
    }

    public function getProductos() {
        $conceptoProductos = Concepto::where('id_venta', $this->id)->where('tipoProducto',1)->get();
        $productos = collect([]);

        foreach($conceptoProductos as $concepto) {
            $producto = Producto::find($concepto->idItem);
            $productos->push($producto);
        }
        return productos;
    }
}
