@extends('layouts.appHome')
@section('titulo', 'Consultas')
@section('contenido')
@include('animales._menuAnimal')

<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">CONSULTAS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No. ID</th>
          <th>Paciente</th>
          <th>Fecha / Hora</th>
          <th>Médico</th>
        </tr>
      </thead>
      <tbody>
      @foreach($animal->consultas as $consulta)
        <tr>
          <td>{{ $consulta->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$consulta->animal->id]) }}">  
              {{ $consulta->animal->nombre }}
            </a>
          </td>
          <td>{{ $consulta->created_at }}</td>
          <td>{{ $consulta->user->name }}</td>
          @endforeach
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DE LA INTERNACIÓN</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No. ID</th>
          <th>Paciente</th>
          <th>Motivo</th>
          <th>Fecha de Ingreso</th>
          <th>Fecha de Salida</th>
        </tr>
      </thead>
      <tbody>
      @foreach($animal->internaciones as $internacion)
        <tr>
          <td>{{ $internacion->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$animal->id]) }}">  
              {{ $internacion->animal->nombre }}
            </a>
          </td>
          <td>{{ $internacion->motivo }}</td>
          <td>{{ $internacion->fechaIngreso }}</td>
          <td>{{ $internacion->fechaSalida }}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LABORATORIOS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No. ID</th>
          <th>Paciente</th>
          <th>Tipo Examen</th>
          <th>Muestra</th>
        </tr>
      </thead>
      <tbody>
      @foreach($animal->laboratorios as $laboratorio)
        <tr>
          <td>{{ $laboratorio->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$laboratorio->animal->id]) }}">  
              {{ $laboratorio->animal->nombre }}
            </a>
          </td>
          <td>{{ $laboratorio->tipoExamen }}</td>
          <td>{{ $laboratorio->muestra }}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">VACUNAS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-hover">
      <thead>
        <tr>
          <th>No. ID</th>
          <th>Paciente</th>
          <th>Nombre Vacuna</th>
          <th>Fecha Aplicacion</th>
        </tr>
      </thead>
      <tbody>
      @foreach($animal->vacunas as $vacuna)
        <tr>
          <td>{{ $vacuna->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$vacuna->animal->id]) }}">  
              {{ $vacuna->animal->nombre }}
            </a>
          </td>
          <td>{{ $vacuna->nombre }}</td>
          <td>{{ $vacuna->fechaAplicacion }}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
</div>

@endsection

