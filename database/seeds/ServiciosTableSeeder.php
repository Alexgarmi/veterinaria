<?php

use Illuminate\Database\Seeder;
use Veterinaria\Servicio;

class ServiciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servicio = new Servicio();
        $servicio->nombreServicio = 'Peluqueria';
        $servicio->precio = 20.0;
        $servicio->save();

        $servicio = new Servicio();
        $servicio->nombreServicio = 'Baño';
        $servicio->precio = 15.0;
        $servicio->save();
    }
}
