<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'internaciones' ? 'active' : '' }}">
    <a href="{{ route('internaciones_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Internaciones</span>
    </a>
  </li>
<!--
  @ if(Auth::user()->hasAnyRole(['administrador']))
  <li class="{ { Request::path() == 'internaciones' ? 'disabled text-info' : '' }}">
    <a href="#">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Internacion</span>
    </a>
  </li>
  @ endif
-->  
</ul>
