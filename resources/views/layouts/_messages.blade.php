@if( session()->has('message'))
<div class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>{{ session()->get('message') }}</strong>
</div>
@endif

@if( session()->has('message-error'))
<div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>{{ session()->get('message-error') }}</strong>
</div>
@endif
