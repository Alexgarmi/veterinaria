<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Internacion;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateInternacionRequest;
use Veterinaria\Http\Requests\UpdateClienteRequest;

class InternacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $internaciones = Internacion::orderBy('id', 'desc')->paginate(4);

        return view('internaciones.index')->with(['internaciones' => $internaciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animales = Animal::all();

        return view('internaciones.select')->with(['animales' => $animales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFor(Animal $animal)
    {
        $internacion = new Internacion;
        $internacion->id_animal = $animal->id;

        return view('internaciones.create')->with(['internacion' => $internacion])->with(['animal' => $animal]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateInternacionRequest $request)
    {
        $internacion = new Internacion;
        $internacion->motivo = $request->get('motivo');
        $internacion->fechaIngreso = $request->get('fechaIngreso');
        $internacion->fechaSalida = $request->get('fechaSalida');
        $internacion->pendiente = true;
        $internacion->id_animal = $request->get('id_animal');
        $internacion->id_user = $request->user()->id;

        $internacion->save();

        session()->flash('message', 'La internacion fue agregado a la Base de Datos!');

        return redirect()->route('animal_path', ['animal' => $internacion->id_animal]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Internacion $internacion)
    {
        return view('internaciones.show')->with(['internacion' => $internacion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Internacion $internacion)
    {
        return view('internaciones.edit')->with(['internacion' => $internacion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateInternacionRequest $request, Internacion $internacion)
    {
        $internacion->update(
            $request->only('motivo', 'fechaIngreso', 'fechaSalida')
        );

        session()->flash('message', 'Se actualizó la información de la internacion!');

        return redirect()->route('internacion_path',['internacion' => $internacion->id]);
    }
}
