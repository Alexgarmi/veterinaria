<ul class="nav nav-tabs">

@if(Auth::user()->hasAnyRole(['administrador','almacen']))
  <li class="{{ Request::path() == 'productos/create' ? 'active' : '' }}">
  	<a href="{{ route('create_producto_path')}}">
  	<span class="text-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo Producto</span>
  	</a>
  </li>
@endif

  <li class="{{ Request::path() == 'productos' ? 'active' : '' }}">
  	<a href="{{ route('productos_path')}}">
  	<span class="text-primary"><i class="fa fa-th-list" aria-hidden="true"></i> Listar Productos</span>
  	</a>
  </li>



</ul>
