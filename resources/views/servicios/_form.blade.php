<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">SERVICIOS</h3>
  </div>
  <div class="panel-body">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">Ingresar Datos del Servicio:</div>
            <div class="panel-body">
              @if( $servicio->
              exists )
              <form class="form-horizontal" action="{{ route('update_servicio_path',['servicio' =>$servicio->id]) }}" method="POST" role="form">
                {{ method_field('PUT') }}
                @else
                <form class="form-horizontal" action="{{ route('store_servicios_path') }}" method="POST" role="form">
                  @endif
                  {{ csrf_field() }}
                  <fieldset>
                    <div class="form-group">
                      <label for="nombreServicio" class="col-lg-2 control-label">Nombre Servicio</label>
                      <div class="col-lg-10">
                        <input type="text" class="form-control" name="nombreServicio" value="{{ $servicio->nombreServicio or old('nombreServicio') }}" placeholder="Nombre del servicio">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="precio" class="col-lg-2 control-label">Precio</label>
                      <div class="col-lg-10">
                        <input type="text" class="form-control" name="precio" value="{{ $servicio->precio or old('precio') }}" placeholder="Precio del servicio">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-lg-10 col-lg-offset-2">
                        <a href="{{ route('servicios_path') }}" class="btn btn-default">Cancelar</a>
                        <button type="submit" class="btn btn-primary">Guardar Servicio</button>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
