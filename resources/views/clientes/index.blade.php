@extends('layouts.appHome')
@section('titulo', 'Clientes')
@section('contenido')
@include('clientes._menu')



<div class="panel panel-info">
  <div class="panel-heading" align="center">
    <h3 class="panel-title" align="center">LISTADO DE CLIENTES</h3>
  </div>
  <div class="panel-body table-responsive">
  @include('layouts._messages')
  @include('layouts._errors')
    <table class="table table-hover table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Cliente</th>
          <th>Teléfono</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($clientes as $cliente)
        <tr>
          <td>{{ $cliente->id }}</td>
          <td>
              <a href="{{ route('cliente_path',['cliente' =>$cliente->id]) }}">
                {{ $cliente->nombre }}
              </a>
          </td>
          <td>{{ $cliente->telefono }}</td>
          <td>
           @if(count($cliente->animales)==0)
            <form action="{{ route('delete_cliente_path',['cliente'=>$cliente->id]) }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-link btn-xs">
                <spam class="text-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</spam>
              </button>
            </form>
            @else
              <button class="btn btn-link disabled btn-xs">
                <spam class="text-muted"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</spam>
              </button>
            @endif
          </td>
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $clientes->render() }}
  </div>
</div>
@endsection

