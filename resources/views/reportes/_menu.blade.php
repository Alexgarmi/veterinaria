<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'reportes/dia' ? 'active' : '' }}">
    <a href="{{ route('reporte_dias_path') }}">
      <span class="text-primary"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> Ventas por Dia</span>
      </a>
    </li>
    <li class="{{ Request::path() == 'reportes/mes' ? 'active' : '' }}">
      <a href="{{ route('reporte_mes_path') }}">
     <span class="text-primary"><i class="fa fa-calendar" aria-hidden="true"></i> Ventas por Mes</span>
      </a>
    </li>
    <li class="{{ Request::path() == 'reportes/anyo' ? 'active' : '' }}">
      <a href="{{ route('reporte_anyo_path') }}">
      <span class="text-primary"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> entas por Año</span>
      </a>
    </li>
  </ul>
