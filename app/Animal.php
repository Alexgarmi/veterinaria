<?php

namespace Veterinaria;

use Veterinaria\Cliente;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Animal extends Model{

	protected $table = 'animales';

	protected $filelable = ['nombre', 'especie', 'raza', 'genero', 'fechaNacimiento', 'id_cliente'];

	protected $guarded = array();

	public function cliente()
	{
		return $this->belongsTo('Veterinaria\Cliente', 'id_cliente');
	}

	public function atenciones()
	{
    	return $this->hasMany('Veterinaria\Atencion', 'id_animal');
	}
	
	public function consultas()
	{
		return $this->hasMany('Veterinaria\Consulta', 'id_animal');
	}

	public function internaciones()
	{
		return $this->hasMany('Veterinaria\Internacion', 'id_animal');
	}

	public function vacunas()
	{
		return $this->hasMany('Veterinaria\Vacuna', 'id_animal');
	}

	public function laboratorios()
	{
		return $this->hasMany('Veterinaria\Laboratorio', 'id_animal');
	}
}

?>