<div class="panel panel-primary">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">CREAR / EDITAR INTERNACIÓN</h3>
  </div>
  <div class="panel-body">
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-primary">
        <div class="panel-heading">Ingresar Datos de la Internación - No. {{ $internacion->id }}:</div>
        <div class="panel-body">
          @if( $internacion-> exists )
            <form action="{{ route('update_internacion_path',['internacion' => $internacion->id]) }}" class="form-horizontal" method="POST" role="form">
            {{ method_field('PUT') }}
          @else
            <form action="{{ route('store_internaciones_path') }}" class="form-horizontal" method="POST" role="form">
          @endif
            {{ csrf_field() }}
              <fieldset>
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="motivo">Motivo</label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-wheelchair" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="motivo" name="motivo" placeholder="Motivo de la internacion" type="text" value="{{ $internacion->motivo or old('motivo') }}"/>
                    </div>
                  </div>
                </div>
                
                <div class="form-group ">
                  <label class="control-label col-lg-2" for="fechaIngreso">Fecha de Ingreso</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="fechaIngreso" name="fechaIngreso" placeholder="AAAA-MM-DD" type="date" value="{{ $internacion->fechaIngreso or old('fechaIngreso') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="fechaSalida">Fecha de Salida Estimada</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                      </div>
                      <input class="form-control" id="fechaSalida" name="fechaSalida" placeholder="AAAA-MM-DD" type="date" value="{{ $internacion->fechaSalida or old('fechaSalida') }}"/>
                    </div>
                  </div>
                </div>

                <div class="form-group ">
                  <label class="control-label col-lg-2" for="id_animal">Nombre de la Mascota </label>
                  <div class="col-lg-10">
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                      </div>
                      @if($internacion->exists)
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $internacion->animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $internacion->animal->id or old('id_animal') }}"/>
                      @else
                        <input class="form-control" id="animal" name="animal" placeholder="animal" type="text"  readonly="readonly" value="{{ $animal->nombre}}"/>
                        <input type="hidden" name="id_animal" value="{{ $animal->id or old('id_animal') }}"/>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    @if($internacion->exists)
                      <a href="{{ route('internacion_path',['internacion' => $internacion->id]) }}" class="btn btn-default">Cancelar</a>
                    @else
                      <a href="{{ route('animal_path',['animal' =>$animal->id]) }}" class="btn btn-default">Cancelar</a>
                    @endif
                    <button class="btn btn-primary" type="submit">Guardar Internación</button>
                  </div>
                </div>
              </fieldset>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
  </div>
</div>