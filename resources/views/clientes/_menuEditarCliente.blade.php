<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'clientes' ? 'active' : '' }}">
    <a href="{{ route('create_cliente_path') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo Cliente</a>
  </li>
  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    <a href="{{ route('clientes_path') }}"><i class="fa fa-list-ul" aria-hidden="true"></i> Listar Clientes</a>
  </li>
  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    <a href="{{ route('edit_cliente_path', ['cliente' =>$cliente->id]) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Cliente</a>
  </li>
  <li class="{{ Request::path() == 'clientes/create' ? 'active' : '' }}">
    
    <form action="{{ route('delete_cliente_path',['cliente'=>$cliente->id]) }}" method="POST">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
			<button type="submit" class="btn btn-link"><i class="fa fa-trash-o" aria-hidden="true"></i> <strong> Eliminar Cliente</strong></button>
        </form></li>
 </ul>
<div class="well well-sm" align="center">
  <strong>@yield('tituloCentral')</strong>
</div>

