@extends('layouts.appHome')
@section('titulo', 'Editar Internacion')
@section('contenido')
@section('tituloCentral', 'EDITAR INTERNACION')
@include('vacunas._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('vacunas._form',['vacuna' => $vacuna])
@endsection