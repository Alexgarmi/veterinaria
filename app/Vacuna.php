<?php

namespace Veterinaria;

use Veterinaria\Animal;
use Veterinaria\DatesTranslator;
use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model{

	use DatesTranslator;

	protected $table = 'vacunas';

	protected $fillable = ['nombre', 'fechaAplicacion', 'aplicado', 'pendiente', 'id_animal', 'id_user'];

	public function animal()
	{
		return $this->belongsTo('Veterinaria\Animal', 'id_animal');
	}
}