<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\User;
use Veterinaria\Role;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::orderBy('id', 'desc')->paginate(4);

        return view('usuarios.index')->with(['usuarios' => $usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usuario = new User;
        $roles = Role::all();

        return view('usuarios.create')->with(['usuario' => $usuario])->with(['roles' => $roles]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $user = User::create([
            'name' => $request->get('nombreUsuario'),
            'email' => $request->get('emailUsuario'),
            'password' => bcrypt($request->get('passwordUsuario')),
        ]);
        $user
       ->roles()
       ->attach(Role::where('id', $request->get('id_rol'))->first());

        session()->flash('message', 'El user fue agregado a la Base de Datos!');

        return redirect()->route('usuarios_path'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id_user
     * @return \Illuminate\Http\Response
     */
    public function show(User $usuario)
    {
       
        return view('usuarios.show')->with(['usuario' => $usuario]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id_user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $usuario)
    {
        $roles = Role::all();

        return view('usuarios.edit')->with(['usuario' => $usuario])->with(['roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $usuario)
    {
        $usuario->name =  $request->get('nombreUsuario');
        $usuario->email =  $request->get('emailUsuario');
        if ($request->get('passwordUsuario')) {
            $usuario->password =  $request->get('passwordUsuario');
        }
        $usuario->save();

        $usuario->roles()->detach();
        $usuario->roles()->attach(Role::where('id', $request->get('id_rol'))->first());

        session()->flash('message', 'Se actualizó la información del usuario!');

        return redirect()->route('usuario_path',['usuario' => $usuario->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(User $user)
    {
        $user->delete();

        session()->flash('message', 'El usuario fue eliminado de la Base de Datos!');

        return redirect()->route('usuarios_path');
    }
}
