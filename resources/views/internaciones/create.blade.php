@extends('layouts.appHome')
@section('titulo', 'Registrar Internacion')
@section('contenido')
@include('internaciones._menu')
@include('layouts._messages')
@include('layouts._errors')
@include('internaciones._form',['internacion' => $internacion, 'animal' => $animal])
@endsection