@extends('layouts.appHome')
@section('titulo', 'Cliente')
@section('contenido')
@include('clientes._menuCliente')
@include('layouts._messages')
@include('layouts._errors')

<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">INFORMACIÓN DEL CLIENTE</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-condensed">
      <tbody>
        <tr>
          <td width="100"><strong>No. ID:</strong></td>
          <td>{{ $cliente->id }}</td>
          <td width="150"></td>
          <td></td>
        </tr>
        <tr>
          <td><strong>Nombre:</strong></td>
          <td>{{ $cliente->nombre }}</td>
          <td><strong>Teléfono:</strong></td>
          <td>{{ $cliente->telefono }}</td>
        </tr>
        <tr>
          <td><strong>CI ó NIT:</strong></td>
          <td>{{ $cliente->ci }}</td>
          <td><strong>Dirección:</strong></td>
          <td>{{ $cliente->direccion }}</td>
       </tr>
        <tr>
          <td><strong>e-mail:</strong></td>
          <td>{{ $cliente->email }}</td>
          <td><strong>Fecha de registro:</strong></td>
          <td>{{ $cliente->created_at }}</td>
         </tr>
      </tbody>
    </table>
  </div>
</div>
@if(Auth::user()->hasAnyRole(['administrador','recepcion']))
<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">ANIMALES DEL CLIENTE</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-condensed table-striped table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nombre</th>
          <th>Especie / Raza</th>
          <th colspan="2">Opciones</th>
        </tr>
      </thead>
      <tbody>
      @foreach($cliente->animales as $animal)
        <tr>
          <td>{{ $animal->id }}</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$animal->id]) }}">
              {{ $animal->nombre }} 
            </a>
          </td>
          <td>{{ $animal->especie }} / {{ $animal->raza }}</td>
          <td><a href="#" class="btn btn-link btn-xs"><i class="fa fa-folder-o" aria-hidden="true"></i> Historia</a></td>
          <td>
            <form action="{{ route('delete_animal_from_cliente_path',['animal'=>$animal, 'cliente'=>$cliente]) }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-link btn-xs">
                <spam class="text-danger">
                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                  Eliminar
                </spam>
              </button>
            </form>
          </td>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @if($cliente)
<a href="{{ route('create_animal_to_cliente_path', ['cliente' => $cliente]) }}" class="btn btn-link">
<i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Animal</a>
@endif
</div>
</div>
@endif
@endsection
