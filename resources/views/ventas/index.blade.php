@extends('layouts.appHome')
@section('titulo', 'Ventas')
@section('contenido')
@include('ventas._menu')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">LISTADO DE VENTAS</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-hover ">
      <thead>
        <tr>
          <th>#</th>
          <th>Cliente</th>
          <th>Fecha</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ventas as $venta)
        <tr>
          <td>{{ $venta->id }}</td>
          <td>
              <a href="{{ route('venta_path',['venta' =>$venta->id]) }}">
                {{ $venta->cliente->nombre }}
              </a>
          </td>
          <td>{{ $venta->fechaPago }}</td>
          <td with="100" align="right">{{ $venta->total }}</td>
          <td></td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{ $ventas->render() }}
    @endsection
  </div>
</div>
