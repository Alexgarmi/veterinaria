<?php

namespace Veterinaria\Http\Controllers;

use Illuminate\Http\Request;
use Veterinaria\Cliente;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function login()
    {
        return view('webCliente.login');
    }

    public function indexWeb(Request $request)
    {
        $inputEmail = $request->get('inputEmail');

        $cliente = Cliente::where('email', $inputEmail)->first();

        if ($cliente) {
            return view('webCliente.index')->with(['cliente' => $cliente]);
        } else {
            session()->flash('message', 'El cliente con email '. $inputEmail . ' no esta registrado.');
            return view('webCliente.login');
        }
    }
}
