<?php

namespace Veterinaria\Http\Controllers;

use Veterinaria\Animal;
use Veterinaria\Cliente;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateAnimalRequest;
use Veterinaria\Http\Requests\UpdateAnimalRequest;

class AnimalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animales = Animal::orderBy('id', 'desc')->paginate(4);

        return view('animales.index')->with(['animales' => $animales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animal = new Animal;

        return view('animales.create')->with(['animal' => $animal]);
    }

    public function createWithCliente(Cliente $cliente)
    {
        $animal = new Animal;
        return view('animales.create')->with(['animal' => $animal, 'cliente' => $cliente]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(CreateAnimalRequest $request)
    {
        
        $animal = new Animal;
        $animal->fill(
            $request->only('nombre', 'especie', 'raza', 'genero', 'fechaNacimiento', 'id_cliente')
        );
        //$animal->id_cliente = $request->get(id_cliente);
        $animal->save();

        session()->flash('message', 'El animal fue agregado a la Base de Datos!');


        return redirect()->route('animales_path'); 
    }
    

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Animal $animal)
    {
        return view('animales.show')->with(['animal' => $animal]);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Animal $animal)//, Cliente $cliente)
    {
        return view('animales.edit')->with(['animal' => $animal, 'cliente' => $animal->cliente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAnimalRequest $request, Animal $animal)
    {
        $animal->update(
            $request->only('nombre', 'especie', 'raza', 'genero', 'fechaNacimiento', 'id_cliente')
        );

        session()->flash('message', 'Se actualizó la información del animal!');

        return redirect()->route('animal_path',['animal' => $animal->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Animal $animal)
    {
        $animal->delete();

        session()->flash('message', 'Animal eliminado de la Base de Datos!');

        return redirect()->route('animales_path');
    }
    
    public function deleteFromCliente(Animal $animal, Cliente $cliente)
    {
        $animal->delete();
        session()->flash('message', 'Animal eliminado de la Base de Datos!');
        return redirect()->route('cliente_path',['cliente' => $cliente->id]);
    }          
}
