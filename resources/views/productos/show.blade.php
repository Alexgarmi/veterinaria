@extends('layouts.appHome')

@section('titulo', 'Producto')
@section('contenido')
@include('productos._menuProducto')
@include('layouts._messages')
@include('layouts._errors')

<div class="panel panel-success">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">INFORMACIÓN DEL PRODUCTO</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-condensed">
      <tbody>
        <tr>
          <td width="200"><strong>No. ID:</strong></td>
          <td>{{ $producto->id }}</td>
        </tr>
        <tr>
          <td><strong>Nombre:</strong></td>
          <td>{{ $producto->nombre }}</td>
        </tr>
        <tr>
          <td><strong>Precio:</strong></td>
          <td>{{ $producto->precio }}</td>
       </tr>
        <tr>
          <td><strong>Stock:</strong></td>
          <td>{{ $producto->stock }}</td>
        </tr>
          <td><strong>Fecha de registro:</strong></td>
          <td>{{ $producto->created_at->diffForHumans() }}</td>
         </tr>
      </tbody>
    </table>
  </div>
</div>

@endsection