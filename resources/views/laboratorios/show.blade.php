@extends('layouts.appHome')
@section('titulo', 'Vacuna')
@section('contenido')
@include('laboratorios._menuEditar')
@include('layouts._messages')
@include('layouts._errors')
<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">INFORMACIÓN DEL LABORATORIO</h3>
  </div>
  <div class="panel-body table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <tr>
          <td width="300">No. ID:</td>
          <td>{{ $laboratorio->id }}</td>
        </tr>
        <tr>
          <td>Tipo Examen:</td>
          <td>{{ $laboratorio->tipoExamen }}</td>
        </tr>
        <tr>
          <td>Muestra:</td>
          <td>{{ $laboratorio->muestra }}</td>
        </tr>
        <tr>
          <td>Resultado:</td>
          <td>{{ $laboratorio->resultado }}</td>
        </tr>
        <tr>
          <td>Animal:</td>
          <td>
            <a href="{{ route('animal_path',['animal' =>$laboratorio->animal->id]) }}">
              {{ $laboratorio->animal->id }}: {{ $laboratorio->animal->nombre }}
            </a>
          </td>
        </tr>
        <tr>
          <td>Registrado desde:</td>
          <td>{{ $laboratorio->created_at->diffForHumans() }}</td>
        </tr>
      </div>
    </div>
    </tbody>
    </table>
    </div>
    </div>

    @endsection
