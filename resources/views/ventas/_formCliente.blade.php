<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title" align="center">CREAR NUEVA VENTA</h3>
  </div>
  <div class="panel-body">

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Datos de la Venta:</div>
        <div class="panel-body">
          <form class="form-horizontal" action="{{ route('add_producto_save_venta_path') }}" method="POST" role="form">
            {{ csrf_field() }}
            <fieldset>
              <div class="form-group ">
                <label class="control-label col-lg-2" for="id_cliente">Cliente</label>
                <div class="col-lg-10">
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                    <select class="select form-control" id="id_cliente" name="id_cliente">
                      @foreach($clientes as $cliente)
                      <option value="{{ $cliente->id }}">
                        {{ $cliente->nombre }}
                        @endforeach
                      </option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <button type="submit" class="btn btn-primary">Seleccionar Cliente</button>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


  </div>
</div>





