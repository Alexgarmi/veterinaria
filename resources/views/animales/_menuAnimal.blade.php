<ul class="nav nav-tabs">
<!--
  <li class="{ { Request::path() == 'animales/create' ? 'active' : '' }}">
    <a href="{ { route('create_animal_path') }}"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Animal</a>
  </li>
-->
  <li class="{{ Request::path() == 'animales' ? 'active' : '' }}">
    <a href="{{ route('animales_path') }}">
    <span class="text-primary"> <i class="fa fa-list-ul" aria-hidden="true"></i> Listar Animales</a></span>
  </li>

  <li class="{{ Request::path() == 'animales/create' ? 'active' : '' }} ">
    <a href="{{ route('edit_animal_path', ['animal' =>$animal->id]) }}">
    <span class="text-primary"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Animal</a></span>
  </li>

  <li class="{{ Request::path() == 'consultas/historial' ? 'active' : '' }} ">
    <a href="{{ route('animal_historial_path', ['animal' =>$animal->id]) }}"> 
    <span class="text-primary"> <i class="fa fa-folder-o" aria-hidden="true"></i> Historial</a></span>
  </li>
  
  @if(Auth::user()->hasAnyRole(['administrador','veterinario']))
  <li class="{{ Request::path() == 'consultas/create' ? 'active' : '' }} ">
    <a href="{{ route('create_consulta_path', ['animal' =>$animal->id]) }}">
    <span class="text-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Consulta</a></span>
  </li>
  @endif

  @if(Auth::user()->hasAnyRole(['administrador','veterinario']))
  <li class="{{ Request::path() == 'internaciones/create' ? 'active' : '' }} ">
    <a href="{{ route('create_internacion_path', ['animal' =>$animal->id]) }}">
    <span class="text-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Internación</a></span>
  </li>
  @endif

  <li class="{{ Request::path() == 'laboratorios/create' ? 'active' : '' }} ">
    <a href="{{ route('create_laboratorio_animal_path', ['animal' =>$animal->id]) }}">
    <span class="text-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Laboratorio</a></span>
  </li>

  @if(Auth::user()->hasAnyRole(['administrador','veterinario']))
  <li class="{{ Request::path() == 'vacunas/create' ? 'active' : '' }} ">
    <a href="{{ route('create_vacuna_animal_path', ['animal' =>$animal->id]) }}">
    <span class="text-primary"> <i class="fa fa-plus" aria-hidden="true"></i> Vacuna</a></span>
  </li>
  @endif

  <li class="{{ Request::path() == 'animales/create' ? 'active' : '' }}">
    <form action="{{ route('delete_animal_path',['animal'=>$animal->id]) }}" method="POST">
          {{ csrf_field() }}
          {{ method_field('DELETE') }}
			<button type="submit" class="btn btn-link text-danger">
      <span class="text-primary"> <i class="fa fa-trash-o" aria-hidden="true"></i>  Eliminar Animal</spam>
      </button>
    </form>
  </li>

 </ul>
