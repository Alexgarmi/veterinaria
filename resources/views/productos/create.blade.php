@extends('layouts.appHome')

@section('titulo', 'Crear Producto')

@section('contenido')
@include('productos._menu')
@include('layouts._messages')
@include('layouts._errors')
	
	@include('productos._form',['producto' => $producto])

@endsection