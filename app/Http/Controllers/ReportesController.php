<?php

namespace Veterinaria\Http\Controllers;
 
use Veterinaria\Venta;
use Veterinaria\Cliente;
use Veterinaria\Producto;
use Veterinaria\Concepto;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateVentaRequest;
use DB;

class ReportesController extends Controller
{
    public function dia()
    {
        $ventas = Venta::select(DB::raw('count(id) as numero, sum(total) as `data`'),DB::raw('DAY(fechaPago) day, MONTH(fechaPago) month, YEAR(fechaPago) year'))
           ->whereNotNull('fechaPago')
           ->groupby('day','month', 'year')
           ->get();

        return view('reportes.dias')->with(['ventas' => $ventas]);
    }

    public function mes()
    {
        $ventas = Venta::select(DB::raw('count(id) as numero, sum(total) as `data`'),DB::raw('MONTH(fechaPago) month, YEAR(fechaPago) year'))
           ->whereNotNull('fechaPago')
           ->groupby('month', 'year')
           ->get();

        return view('reportes.meses')->with(['ventas' => $ventas]);
    }

    public function anyo()
    {
        $ventas = Venta::select(DB::raw('count(id) as numero, sum(total) as `data`'),DB::raw('YEAR(fechaPago) year'))
           ->whereNotNull('fechaPago')
           ->groupby('year')
           ->get();

        return view('reportes.anyos')->with(['ventas' => $ventas]);
    }
}