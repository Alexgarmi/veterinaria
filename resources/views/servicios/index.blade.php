@extends('layouts.appHome')
@section('titulo', 'Servicios')
@section('contenido')
@include('servicios._menu')
<div class="panel panel-success">
  <div class="panel-heading"  align="center">
    <h3 class="panel-title" align="center">LISTADO DE SERVICIOS</h3>
  </div>
  <div class="panel-body table-responsive">
    @include('layouts._messages')
    @include('layouts._errors')
    <table class="table table-hover table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre Servicio</th>
          <th>Precio</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($servicios as $servicio)
        <tr>
          <td>{{ $servicio->id }}</td>
          <td>{{ $servicio->nombreServicio }}</td>
          <td>{{ $servicio->precio }}</td>
          <td>
            <span>
              @if(Auth::user()->hasAnyRole(['administrador']))
              <a href="{{ route('edit_servicio_path', ['servicio' =>$servicio->id]) }}" class="btn btn-info btn-link btn-xs">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                Editar
              </a>
              @endif
            </span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
{{ $servicios->render() }}
</div>
</div>

@endsection
