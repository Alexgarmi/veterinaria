@extends('layouts.appHome')

@section('titulo', 'Venta')
@section('contenido')
@section('tituloCentral', 'INFORMACIÓN DE LA VENTA')
@include('reportes._menu')
@include('layouts._messages')
@include('layouts._errors')	

<div class="panel panel-success">
  <div class="panel-heading" align="center">
    <h3 class="panel-title">VENTAS POR MES</h3>
  </div>
  <div class="panel-body table-responsive">

    <table class="table table-striped table-hover " >
    <thead>
        <tr>
        <th with="100">Mes</th>
        <th with="100">Ventas</th>
        <th align="right">Total</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ventas as $venta)
        <tr>
        <td>{{ $venta->month.'/'.$venta->year }}</td>
        <td>{{ $venta->numero }}</td>
        <td>{{ $venta->data }}</td>
        </tr>
    @endforeach
    </tbody>
    </table>
  </div>
</div>

@endsection