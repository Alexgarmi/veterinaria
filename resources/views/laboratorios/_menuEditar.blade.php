<ul class="nav nav-tabs">
  <li class="{{ Request::path() == 'laboratorios' ? 'active' : '' }}">
    <a href="{{ route('laboratorios_path') }}">
      <span class="text-primary"><i class="fa fa-list-ul" aria-hidden="true"></i>
      Listar Laboratorios</span>
    </a>
  </li>
@if(Auth::user()->hasAnyRole(['administrador','veterinario']))  
  <li class="{{ Request::path() == 'laboratorios/'.$laboratorio->id.'/edit' ? 'active' : '' }}">
    <a href="{{ route('edit_laboratorio_path', ['laboratorio' =>$laboratorio->id]) }}">
      <span class="text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
      Editar Laboratorio</span>
    </a>
  </li>
@endif
</ul>
