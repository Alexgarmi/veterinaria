<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('index_path')->get('/', 'IndexController@index');

Route::name('webCliente_index_path')->get('/webCliente', 'IndexController@indexWeb');
Route::name('webCliente_path')->get('/webCliente/login', 'IndexController@login');


Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::get('/home', 'HomeController@index');

//****** VISTAS CLIENTES ********//	
Route::name('clientes_path')->get('/clientes', 'ClientesController@index');
Route::name('create_cliente_path')->get('/clientes/create', 'ClientesController@create');
Route::name('store_clientes_path')->post('/clientes', 'ClientesController@store');
Route::name('cliente_path')->get('/clientes/{cliente}', 'ClientesController@show');
Route::name('edit_cliente_path')->get('/clientes/{cliente}/edit', 'ClientesController@edit');
Route::name('update_cliente_path')->put('/clientes/{cliente}', 'ClientesController@update');
Route::name('delete_cliente_path')->delete('/clientes/{cliente}', 'ClientesController@delete');

//****** VISTAS ANIMALES ********//	
Route::name('animales_path')->get('/animales', 'AnimalesController@index');
Route::name('create_animal_path')->get('/animales/create/{cliente}', 'AnimalesController@create');
Route::name('store_animales_path')->post('/animales', 'AnimalesController@store');
Route::name('animal_path')->get('/animales/{animal}', 'AnimalesController@show');
Route::name('edit_animal_path')->get('/animales/{animal}/edit', 'AnimalesController@edit');
Route::name('update_animal_path')->put('/animales/{animal}', 'AnimalesController@update');
Route::name('delete_animal_path')->delete('/animales/{animal}', 'AnimalesController@delete');

Route::name('delete_animal_from_cliente_path')->delete('/animales/{animal}/{cliente}', 'AnimalesController@deleteFromCliente');
Route::name('create_animal_to_cliente_path')->get('/animales/create/{cliente}', 'AnimalesController@createWithCliente');

//****** VISTAS CONSULTAS ********//	
Route::name('consultas_path')->get('/consultas', 'ConsultasController@index');
Route::name('create_consulta_path')->get('/consultas/create/{animal}', 'ConsultasController@create');

Route::name('store_consultas_path')->post('/consultas', 'ConsultasController@store');
Route::name('consulta_path')->get('/consultas/{consulta}', 'ConsultasController@show');

Route::name('update_consulta_path')->put('/consultas/{consulta}', 'ConsultasController@update');

//****** VISTAS INTERNACIONES ****//
Route::name('internaciones_path')->get('/internaciones', 'InternacionesController@index');
Route::name('create_internacion_animal_path')->get('/internaciones/create/{animal}', 'InternacionesController@createFor');
Route::name('store_internaciones_path')->post('/internaciones', 'InternacionesController@store');
Route::name('internacion_path')->get('/internaciones/{internacion}', 'InternacionesController@show');
Route::name('edit_internacion_path')->get('/internaciones/{internacion}/edit', 'InternacionesController@edit');
Route::name('update_internacion_path')->put('/internaciones/{internacion}', 'InternacionesController@update');

//****** VISTAS VACUNAS ****//
Route::name('vacunas_path')->get('/vacunas', 'VacunasController@index');
Route::name('create_vacuna_animal_path')->get('/vacunas/create/{animal}', 'VacunasController@createFor');
Route::name('store_vacunas_path')->post('/vacunas', 'VacunasController@store');
Route::name('vacuna_path')->get('/vacunas/{vacuna}', 'VacunasController@show');
Route::name('edit_vacuna_path')->get('/vacunas/{vacuna}/edit', 'VacunasController@edit');
Route::name('update_vacuna_path')->put('/vacunas/{vacuna}', 'VacunasController@update');

//****** VISTAS LABORATORIOS ****//
Route::name('laboratorios_path')->get('/laboratorios', 'LaboratoriosController@index');
Route::name('create_laboratorio_animal_path')->get('/laboratorios/create/{animal}', 'LaboratoriosController@createFor');
Route::name('store_laboratorios_path')->post('/laboratorios', 'LaboratoriosController@store');
Route::name('laboratorio_path')->get('/laboratorios/{laboratorio}', 'LaboratoriosController@show');
Route::name('edit_laboratorio_path')->get('/laboratorios/{laboratorio}/edit', 'LaboratoriosController@edit');
Route::name('update_laboratorio_path')->put('/laboratorios/{laboratorio}', 'LaboratoriosController@update');

//****** VISTAS PRODUCTOS ********//	
Route::name('productos_path')->get('/productos', 'ProductosController@index');
Route::name('create_producto_path')->get('/productos/create', 'ProductosController@create');
Route::name('store_productos_path')->post('/productos', 'ProductosController@store');
Route::name('update_producto_path')->put('/productos/{producto}', 'ProductosController@update');
Route::name('producto_path')->get('/productos/{producto}', 'ProductosController@show');
Route::name('edit_producto_path')->get('/productos/{producto}/edit', 'ProductosController@edit');
Route::name('delete_producto_path')->delete('/productos/{producto}', 'ProductosController@delete');	

//****** VISTAS VENTAS ********//	
Route::name('ventas_path')->get('/ventas', 'VentasController@index');
Route::name('create_venta_path')->get('/ventas/create', 'VentasController@create');
Route::name('add_producto_save_venta_path')->post('/ventas/addProducto/', 'VentasController@addProductoSaveVenta');
Route::name('add_producto_venta_path')->get('/ventas/addProducto/{venta}', 'VentasController@addProducto');
Route::name('select_producto_venta_path')->get('/ventas/selectProducto/{venta}', 'VentasController@selectProducto');
Route::name('store_producto_path')->post('/ventas/venta/{venta}/producto/{producto}', 'VentasController@storeProducto');

Route::name('store_ventas_path')->post('/ventasstore', 'VentasController@store');
Route::name('venta_path')->get('/ventas/{venta}', 'VentasController@show');

//****** VISTAS SERVICIOS ********//	
Route::name('servicios_path')->get('/servicios', 'ServiciosController@index');
Route::name('create_servicio_path')->get('/servicios/create', 'ServiciosController@create');
Route::name('store_servicios_path')->post('/servicios', 'ServiciosController@store');
Route::name('update_servicio_path')->put('/servicios/{servicio}', 'ServiciosController@update');
Route::name('edit_servicio_path')->get('/servicios/{servicio}/edit', 'ServiciosController@edit');


//****** VISTAS ATENCIONES ********//	
Route::name('atenciones_path')->get('/atenciones', 'AtencionesController@index');
Route::name('create_atencion_path')->get('/atenciones/create', 'AtencionesController@create');
Route::name('store_atenciones_path')->post('/atenciones', 'AtencionesController@store');
});


//****** VISTAS USUARIOS ********//	
Route::name('usuarios_path')->get('/usuarios', 'UsuariosController@index');
Route::name('create_usuario_path')->get('/usuarios/create', 'UsuariosController@create');
Route::name('store_usuarios_path')->post('/usuarios', 'UsuariosController@store');
Route::name('update_usuario_path')->put('/usuarios/{usuario}', 'UsuariosController@update');
Route::name('usuario_path')->get('/usuarios/{usuario}', 'UsuariosController@show');
Route::name('edit_usuario_path')->get('/usuarios/{usuario}/edit', 'UsuariosController@edit');
Route::name('delete_usuario_path')->delete('/usuarios/{usuario}', 'UsuariosController@delete');



