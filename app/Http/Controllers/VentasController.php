<?php

namespace Veterinaria\Http\Controllers;
 
use Veterinaria\Venta;
use Veterinaria\Cliente;
use Veterinaria\Producto;
use Veterinaria\Atencion;
use Veterinaria\Consulta;
use Veterinaria\Concepto;
use Veterinaria\Internacion;
use Veterinaria\Vacuna;
use Illuminate\Http\Request;
use Veterinaria\Http\Requests\CreateVentaRequest;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ventas = Venta::whereNotNull('fechaPago')->paginate(4);

        return view('ventas.index')->with(['ventas' => $ventas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::all();

        return view('ventas.create')->with(['clientes' => $clientes]);
    }

    public function addProductoSaveVenta(CreateVentaRequest $request)
    {
        $venta = new Venta;
        
        $venta->id_cliente = $request->get('id_cliente');
        $venta->save();

        //atencion    2    
        //consulta    3  50
        //internacion 4 100
        //vacuna      5  10
        $cliente = Cliente::find($venta->id_cliente);

        foreach($cliente->animales as $animal) {
            foreach($animal->atenciones as $atencion) {
                if($atencion->pendiente) {
                    $concepto = new Concepto;

                    $concepto->idItem = $atencion->id;
                    $concepto->tipoProducto = 2;
                    $concepto->id_venta = $venta->id;

                    $concepto->save();
                }
            }
            foreach($animal->consultas as $consulta) {
                if($consulta->pendienteConsulta) {
                    $concepto = new Concepto;

                    $concepto->idItem = $consulta->id;
                    $concepto->tipoProducto = 3;
                    $concepto->id_venta = $venta->id;

                    $concepto->save();
                }
            }
            foreach($animal->internaciones as $internacion) {
                if($internacion->pendiente) {
                    $concepto = new Concepto;

                    $concepto->idItem = $internacion->id;
                    $concepto->tipoProducto = 4;
                    $concepto->id_venta = $venta->id;

                    $concepto->save();
                }
            }
            foreach($animal->vacunas as $vacuna) {
                if($vacuna->pendiente) {
                    $concepto = new Concepto;

                    $concepto->idItem = $vacuna->id;
                    $concepto->tipoProducto = 5;
                    $concepto->id_venta = $venta->id;

                    $concepto->save();
                }
            }
        }

        //session()->flash('message', 'Cliente seleccionado!');

        return redirect()->route('add_producto_venta_path',['venta' => $venta->id]);
    }

    public function addProducto(Venta $venta)
    {
        $conceptoProductos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',1)->get();
        $productos = collect([]);

        $total = 0;

        foreach($conceptoProductos as $concepto) {
            $producto = Producto::find($concepto->idItem);
            $productos->push($producto);
            $total += $producto->precio;
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',2)->get();
        $atenciones = collect([]);

        foreach($conceptos as $concepto) {
            $atencion = Atencion::find($concepto->idItem);
            $atenciones->push($atencion);
            $total += $atencion->servicio->precio;
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',3)->get();
        $consultas = collect([]);

        foreach($conceptos as $concepto) {
            $consulta = Consulta::find($concepto->idItem);
            $consultas->push($consulta);
            $total += 50;
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',4)->get();
        $internaciones = collect([]);

        foreach($conceptos as $concepto) {
            $internacion = Consulta::find($concepto->idItem);
            $internaciones->push($internacion);
            $total += 100;
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',5)->get();
        $vacunas = collect([]);

        foreach($conceptos as $concepto) {
            $vacuna = Vacuna::find($concepto->idItem);
            $vacunas->push($vacuna);
            $total += 10;
        }

        /*
        foreach($productos as $producto) {
            $total += $producto->precio;
        }
        */

        //session()->flash('message', 'Cliente seleccionado!');
        
        return view('ventas.addProductos')->with(['venta' => $venta, 'productos' => $productos,
            'atenciones' => $atenciones,
            'consultas' => $consultas,
            'internaciones' => $internaciones,
            'vacunas' => $vacunas,
            'total' => $total]);
    }

    public function selectProducto(Venta $venta)
    {
        $productos = Producto::all();
        return view('ventas.selectProducto')->with(['venta' => $venta, 'productos' => $productos]);
    }

    public function storeProducto(Venta $venta, Producto $producto)
    {
        $concepto = new Concepto;

        $concepto->idItem = $producto->id;
        $concepto->tipoProducto = 1;
        $concepto->id_venta = $venta->id;

        $concepto->save();

        return redirect()->route('add_producto_venta_path',['venta' => $venta->id]);
    }

    public function show(Venta $venta)
    {
        $conceptoProductos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',1)->get();
        $productos = collect([]);

        foreach($conceptoProductos as $concepto) {
            $producto = Producto::find($concepto->idItem);
            $productos->push($producto);
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',2)->get();
        $atenciones = collect([]);

        foreach($conceptos as $concepto) {
            $atencion = Atencion::find($concepto->idItem);
            $atenciones->push($atencion);
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',3)->get();
        $consultas = collect([]);

        foreach($conceptos as $concepto) {
            $consulta = Consulta::find($concepto->idItem);
            $consultas->push($consulta);
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',4)->get();
        $internaciones = collect([]);

        foreach($conceptos as $concepto) {
            $internacion = Consulta::find($concepto->idItem);
            $internaciones->push($internacion);
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',5)->get();
        $vacunas = collect([]);

        foreach($conceptos as $concepto) {
            $vacuna = Vacuna::find($concepto->idItem);
            $vacunas->push($vacuna);
        }

        return view('ventas.show')->with(['venta' => $venta, 'productos' => $productos, 'atenciones' => $atenciones,
            'consultas' => $consultas,
            'internaciones' => $internaciones,
            'vacunas' => $vacunas]);
    }

    public function store(Request $request)
    {
        $venta = Venta::find($request->get('id'));
        $venta->fechaPago = \Carbon\Carbon::now();
        $venta->total = $request->get('total');
        $venta->save();

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',2)->get();

        foreach($conceptos as $concepto) {
            $atencion = Atencion::find($concepto->idItem);
            $atencion->pendiente = false;
            $atencion->save();
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',3)->get();

        foreach($conceptos as $concepto) {
            $consulta = Consulta::find($concepto->idItem);
            $consulta->pendienteConsulta = false;
            $consulta->save();
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',4)->get();

        foreach($conceptos as $concepto) {
            $internacion = Consulta::find($concepto->idItem);
            $internacion->pendiente = false;
            $internacion->save();
        }

        $conceptos = Concepto::where('id_venta', $venta->id)->where('tipoProducto',5)->get();

        foreach($conceptos as $concepto) {
            $vacuna = Vacuna::find($concepto->idItem);
            $vacuna->pendiente = false;
            $vacuna->save();
        }

        return redirect()->route('ventas_path');
    }

}