@extends('layouts.app')
@section('titulo', 'Inicio')
@section('contenido')
<br/>
<br/>
<br/>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      @include('layouts._errors')
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-body">
            <form action="{{ route('webCliente_index_path') }}" class="form-horizontal" method="POST" role="form">
              {{ csrf_field() }}
              <fieldset>
                <legend><i class="fa fa-key" aria-hidden="true"></i> Acceso para Clientes</legend>
                @if( session()->has('message'))
                <div class="alert alert-dismissible alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>{{ session()->get('message') }}</strong>
                </div>
                @endif
                <div class="form-group">
                  <label for="inputEmail" class="col-lg-2 control-label">Ingrese su Dirección de Correo Electrónico:</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputEmail" name="inputEmail" placeholder="Email"/>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-10 col-lg-offset-2">
                    <button type="reset" class="btn btn-default">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Acceder</button>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
